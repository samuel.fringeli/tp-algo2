package s26;
import java.awt.Point;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import java.util.ArrayList;

public class PtySearchTree {
  public static final Comparator<Point> VERTICALLY =
          Comparator.<Point>comparingInt(a->a.y).thenComparingInt(a->a.x);
  public static final Comparator<Point> HORIZONTALLY =
          Comparator.<Point>comparingInt(a->a.x).thenComparingInt(a->a.y);
  //============================================================
  static class PtySearchElt {
    final Point ptWithXmedian;
    final Point ptWithYmin;
    public PtySearchElt(Point xMed, Point point) {
      ptWithXmedian = xMed; ptWithYmin = point;
    }
    @Override public String toString() {
      return ""+ptWithXmedian.x+" "+ptWithYmin.x+"-"+ptWithYmin.y;
    }
  }
  //============================================================
  private final BTree<PtySearchElt> tree;

  public PtySearchTree(Point[] points) {
    List<Point> v = new ArrayList<Point>();
    for (Point p: points) v.add(p);
    Collections.sort(v, VERTICALLY);
    tree = build(v);
  }

  public List<Point> search(int xFrom, int xTo, int yTo) {
    List<Point> v = new ArrayList<Point>();
    search(tree.root(), xFrom, xTo, yTo, v);
    return v;
  }

  public String toString() {
    return tree.toReadableString();
  }

  private static List<Point> pointsBefore(List<Point> xP, Point xMid) {
    List<Point> v = new ArrayList<Point>();
    for(Point p:xP) {
      if(HORIZONTALLY.compare(p, xMid) <= 0)
        v.add(p);
    }
    return v;
  }
  private static List<Point> pointsAfter(List<Point> xP, Point xMid) {
    List<Point> v = new ArrayList<Point>();
    for(Point p:xP) {
      if(HORIZONTALLY.compare(p, xMid) > 0)
        v.add(p);
    }
    return v;
  }

  private static void search(BTreeItr<PtySearchElt> z, int xFrom, int xTo, int yTo, List<Point> result) {
    if (z.isBottom()) return;
    PtySearchElt e = z.consult();
    Point p = e.ptWithYmin;
    Point m = e.ptWithXmedian;
    if (p.y > yTo) return;
    if (p.x >= xFrom && p.x <= xTo) result.add(p);
    if (m.x >= xFrom) search(z.left(), xFrom, xTo, yTo, result);
    if (m.x <= xTo) search(z.right(), xFrom, xTo, yTo, result);
  }

  private static BTree<PtySearchElt> build(List<Point> vertSorted) {
    assert (isMonoton(vertSorted, VERTICALLY));
    BTree<PtySearchElt> r = new BTree<PtySearchElt>();
    BTreeItr<PtySearchElt> ri = r.root();
    if (vertSorted.size() == 0) return r;

    if (vertSorted.size() > 1) {
      Point first = vertSorted.remove(0);
      List<Point> horiSorted = new ArrayList<>(vertSorted);
      Collections.sort(horiSorted, HORIZONTALLY);
      Point xMid = horiSorted.get(horiSorted.size()/2);

      ri.insert(new PtySearchElt(xMid, first));
      ri.left().paste(build(pointsBefore(vertSorted, xMid)));
      ri.right().paste(build(pointsAfter(vertSorted, xMid)));
    } else {
      ri.insert(new PtySearchElt(vertSorted.get(0), vertSorted.get(0)));
    }

    return r;
  }

  private static boolean isMonoton(List<Point> v, Comparator<Point> c) {
    boolean ok = true;
    for (int i=1; i<v.size(); i++) {
      ok = ok && c.compare(v.get(i-1), v.get(i)) <= 0;
    }
    return ok;
  }

}
