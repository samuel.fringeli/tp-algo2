S21 - Geometry algorithms
=========================

The archive cgeom_s21.jar contains an application that helps to visually 
verify the behaviour of your s21.Geom.* methods. 

Import that library and add it as a dependency of your project. 
With IntelliJ: File -> Project Structure -> Modules 
                     -> Dependencies -> + -> JARs or directory
                     -> cgeom_s21.jar

Then you can run s21.Geom.main() (which calls s21.CG.main()).

This component supposes that your source code (Geom.java, 
PointComparator.java and Segm.java) is in the s21 package.
