package s12;
import static s12.DoublyLinkedList.ListNode;

public final class DoublyLinkedListItr<E> implements IListItr<E> {
  final DoublyLinkedList<E> list;
  ListNode<E> pred, succ;

  DoublyLinkedListItr(DoublyLinkedList<E> theList) {
    list = theList;
    goToFirst();
  }

  @Override
  public void insertAfter(E e) {
    ListNode<E> n = new ListNode<>(e, pred, succ);
    if (isFirst())
      list.first = n;
    else
      pred.next = n;
    if (isLast())
      list.last = n;
    else
      succ.prev = n;
    succ = n;
    list.size++;
    assert consultAfter()==e;
  }

  @Override
  public void removeAfter() {
    if(isLast()) throw new IllegalStateException("can't removeAfter when isLast");
    if (isFirst())
      list.first = succ.next;
    else
      pred.next = succ.next;
  }

  @Override
  public E consultAfter() {
    if(isLast()) throw new IllegalStateException("can't consultAfter when isLast");
    return succ.elt;
  }

  @Override
  public void goToNext() {
    if(isLast()) throw new IllegalStateException("can't goToNext when isLast");
    pred = succ;
    succ = succ.next;
  }

  @Override
  public void goToPrev() {
    if(isFirst()) throw new IllegalStateException("can't goToPrev when isFirst");
    succ = pred;
    pred = pred.prev;
  }

  @Override
  public void goToFirst() {
    succ = list.first;
    pred = null;
  }

  @Override
  public void goToLast() {
    pred = list.last;
    succ = null;
  }

  @Override
  public boolean isFirst() {
    return null == pred;
  }

  @Override
  public boolean isLast()  {
    return null == succ;
  }
}
