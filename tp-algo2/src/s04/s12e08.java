package s04;

import java.util.Arrays;

/*
Complexity : n * log(n)
*/
public class s12e08 {
  // Search for the longest commun prefix
  public static String lcp(String s, String t) {
    int N = Math.min(s.length(), t.length());
    for (int i = 0; i < N ; i++) {
      if (s.charAt(i) != t.charAt(i)) {
        return s.substring(0,i);
      }
    }
    return s.substring(0,N);
  }

  public static String lrs(String s) {
    int N = s.length();
    String[] suffixes = new String[N];
    for (int i = 0; i < N; i++) {
      suffixes[i] = s.substring(i, N);
    }

    Arrays.sort(suffixes);
    String lrs = "";

    for (int i = 0; i < N - 1; i++) {
      String temp_lcp = lcp(suffixes[i], suffixes[i+1]);
      if (temp_lcp.length() > lrs.length()) {
        lrs = temp_lcp;
      }
    }
    return lrs;
  }

  public static void main(String[] args) {
    String s = "abc defc defc defgh i";
    System.out.println(lrs(s));
  }
}
