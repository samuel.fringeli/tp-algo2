package s22;
import java.awt.Point;
import java.util.*;

//============================================================
public class ClosestPair {
  private static Point crtSolA;
  private static Point crtSolB;
  private static double crtMinDist;
  private static Point[] pt;
  private static int leftMostCandidateIndex;
  //private static BST candidates;   // problem: Point is not Comparable
  private static TreeSet<Point> candidates;

  // ------------------------------------------------------------
  public static Point[] closestPair(Point[] points) {
    pt = new Point[points.length];
    System.arraycopy(points, 0, pt, 0, points.length);
    findClosestPair();
    return (new Point [] {crtSolA,crtSolB});
  }
  // ------------------------------------------------------------
  public static Point[] closestPair(List<? extends Point> points) {
    pt = new Point[points.size()];
    for (int i=0; i<points.size(); i++) {
      Point p = points.get(i);
      pt[i]= p;// new Point(p);
    }
    findClosestPair();
    return (new Point[] {crtSolA, crtSolB});
  }
  // ------------------------------------------------------------
  // IN : points in global array pt
  // OUT: global variables       crtSolA, crtSolB

  private static void findClosestPair() {
    // -- plane sweep  (discrete simulation)
    // At the beginning, closest pair are the two first points
    Arrays.sort(pt, PointComparator.HORIZONTALLY);
    candidates = new TreeSet<>(PointComparator.VERTICALLY);
    Arrays.sort(pt, PointComparator.VERTICALLY);
    crtSolA = pt[0];
    crtSolB = pt[1];
    leftMostCandidateIndex = 0;

    crtMinDist = crtSolA.distance(crtSolB);

    for (Point p : pt) {
      handleEvent(p);
    }


  }
  // ------------------------------------------------------------
  private static void handleEvent(Point p) {
    // consider using: candidates.subSet(pmin, pmax)
    shrinkCandidates(p);
    SortedSet<Point> t= candidates.subSet(new Point((int)(p.x-crtMinDist), (int)(p.y-crtMinDist)),
            new Point((int)(p.x-crtMinDist), (int)(p.y+crtMinDist)));
    Iterator<Point> itr = t.iterator();
    Point x;
    while(itr.hasNext()){
      x = itr.next();
      if(x.distance(p) < crtMinDist){
        crtMinDist = x.distance(p);
        crtSolA = x;
        crtSolB = p;
      }
    }
    candidates.add(p);
  }

  private static void shrinkCandidates(Point p){
    while(p.x - pt[leftMostCandidateIndex].x > crtMinDist){
      candidates.remove(pt[leftMostCandidateIndex]);
      leftMostCandidateIndex++;
    }
  }
}
