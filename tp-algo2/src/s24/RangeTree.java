package s24;
import java.awt.Point;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;

public class RangeTree {
  private static final Comparator<Point> VERTICALLY = 
      Comparator.<Point>comparingInt(a->a.y).thenComparingInt(a->a.x);
  private static final Comparator<Point> HORIZONTALLY = 
      Comparator.<Point>comparingInt(a->a.x).thenComparingInt(a->a.y);
  //============================================================
  // Instead of this class, we can use a lambda expression...
  static class MyIndexComparator implements Comparator<Integer> {
    public  MyIndexComparator(Point[] myArray) {
      // TODO A COMPLETER
    }
    @Override public int compare(Integer a, Integer b) {
      return 0; // TODO A COMPLETER 
    }
  }
  // ============================================================
  @SuppressWarnings("serial")
  static class  BSTElt extends Point implements Comparable<BSTElt> {
    public BSTElt(Point a) {super(a);}
    @Override public int compareTo(BSTElt b) {
      return VERTICALLY.compare(this, b);
    }
  }
  // ============================================================
  static class RangeTreeElt {
    final BST<BSTElt> yTree;  
    final Point       xMedian;
    public RangeTreeElt(BST<BSTElt> t, Point x) {
      yTree = t; xMedian = x;
    }
    @Override public String toString() {
      return ""+xMedian.x;
    }
  }
  // ============================================================
  // ============================================================
  protected final BTree<RangeTreeElt> xTree;  // elts of type RangeTreeElt

  public RangeTree(Point [] points) {
    int n = points.length;
    Point[] xP = new Point [n];
    for (int i=0; i<n; i++) xP[i] = new BSTElt(points[i]);
    Arrays.sort(xP, HORIZONTALLY);
    List<Integer> yOrder = new ArrayList<Integer>(n);
    //  TODO - A COMPLETER...
    xTree = build2dRangeTree(xP, 0, n-1, yOrder);
  }

  public Point[] search(Point bottomLeft, Point topRight) {
    List<Point> v = query2D(xTree.root(), bottomLeft, topRight, 
                            Integer.MIN_VALUE, Integer.MAX_VALUE);
    return v.toArray(new Point[0]);
  }

  public String toString() {
    return ""+xTree.toReadableString(); 
  }
  
  // ------------------------------------------------------------
  // --- Non-public methods
  // ------------------------------------------------------------
  private static List<Point> query1D(BST<BSTElt> ty, int yFrom, int yTo) {
    BSTElt pMin = new BSTElt(new Point(Integer.MIN_VALUE, yFrom));
    BSTElt pMax = new BSTElt(new Point(Integer.MAX_VALUE, yTo  ));
    // TODO - A COMPLETER
    
    return null;
  }

  private static List<Point> query2D(BTreeItr<RangeTreeElt> tx, 
                 Point bottomLeft, Point topRight, int xMin, int xMax) {
    int xFrom = bottomLeft.x; int xTo = topRight.x;
    int yFrom = bottomLeft.y; int yTo = topRight.y;
    List<Point> v = new ArrayList<Point>();
    if (tx.isBottom()) return v;
    RangeTreeElt m = tx.consult();
    int mx = m.xMedian.x; int my = m.xMedian.y;

    // TODO - A COMPLETER

    return v;
  }

  private static BTree<RangeTreeElt> build2dRangeTree(Point[] xP, 
                                        int left, int right, 
                                        List<Integer> yOrder) {
    int n = right+1-left;
    BTree<RangeTreeElt> t = new BTree<RangeTreeElt>();
    BTreeItr<RangeTreeElt> ti=t.root();
    if (n == 0) return t;
    BSTElt[] yElts = new BSTElt[n];
    for (int i=0; i<n; i++) 
      yElts[i] = new BSTElt((xP[yOrder.get(i)]));
    BST<BSTElt> ty = new BST<BSTElt>(yElts);
    int mid = (left+right)/2;
    // TODO - A COMPLETER

    return t;
  }
}
