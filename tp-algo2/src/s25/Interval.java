package s25;

import java.util.Comparator;

// ============================================================
public class Interval {
  public static final Comparator<Interval> STARTING = 
      Comparator.<Interval>comparingInt(a->a.start).thenComparingInt(a->a.end);
  public static final Comparator<Interval> ENDING = 
      Comparator.<Interval>comparingInt(a->a.end).thenComparingInt(a->a.start);
  
  public final int start; // start <= end
  public final int end;
  
  public Interval(int theStart, int theEnd) {
    start = theStart; end = theEnd;
  }
  
  @Override public String toString() {
    return "["+start+"_"+end+"]"; 
  }
  
  @Override public boolean equals(Object o) {
    if(o==null || ! (o instanceof Interval)) return false;
    return STARTING.compare(this, (Interval) o) == 0;
  }
  
  @Override public int hashCode() {
    return 31*start + end;
  }
}
