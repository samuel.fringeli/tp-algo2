package s24;
import java.awt.Point;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import org.junit.Test;
import static org.junit.Assert.*;
public class RangeTreeTestJU {
  // ------------------------------------------------------------
  @Test
  public void testRangeTree() {
    int n=100000; 
    int x1=(int)(Math.sqrt(n)); int y1=x1; 
    int x2=5*x1; int y2=x2;
    int queryRepetitions=1000;
    testRangeTree(n, x1, y1, x2, y2, queryRepetitions, true);
    
    Random r = new Random();
    queryRepetitions=1;
    for(int i=0; i<100; i++) {
      x1=r.nextInt(100);   y1=r.nextInt(100);
      x2=x1+r.nextInt(50); y2=y1+r.nextInt(50);
      testRangeTree(i, x1, y1, x2, y2, queryRepetitions, false);
    }
  }
  // ------------------------------------------------------------
  @Test
  public void testPerformance() {
    testPerformance(50000);
    testPerformance(100000);
    testPerformance(200000);
    testPerformance(400000);
  }
  public void testPerformance(int n) {
    int x1=(int)(Math.sqrt(n)); int y1=x1; 
    int x2=5*x1; int y2=x2;
    int queryRepetitions=1000;
    testRangeTree(n, x1, y1, x2, y2, queryRepetitions, true);
  }
  // ------------------------------------------------------------
  // ---- Test Program
  // ------------------------------------------------------------
  private static int naiveMatching(Point [] t, 
                                   int x1, int x2, int y1, int y2) {
    int count=0;
    for (Point p:t) 
      if (p.x>=x1 && p.x<=x2 && p.y>=y1 && p.y<=y2)
        count++;
    return count;
  }
  // ------------------------------------------------------------
  public static void testRangeTree(int n, int x1, int y1, int x2, int  y2,
                                   int queryRepetitions, boolean log) {
    long t1, t2; int i; 
    // -------------------------------------- Random test 
    myPrint(log, "\nFind range ["+x1+".."+x2+"]["+y1+".."+y2+"]");
    Random r = new Random();
    Point [] t = rndPointSet(r, n, n);
    myPrint(log, " in " +n+" points of coords [0.."+n+"]");
    // -------------------------------------- Building
    t1=System.nanoTime();
    RangeTree rt = new RangeTree(t);
    t2=System.nanoTime();
    myPrint(log, "\n Time[us]: Range Tree building = "+(t2-t1)/1000);
    if (n<50)
      myPrint(log, ""+rt);
    // -------------------------------------- Querying
    Point [] res=null;
    t1=System.nanoTime();
    for(int j=0; j<queryRepetitions; j++)
      res  = rt.search(new Point(x1, y1), new Point(x2, y2));
    t2=System.nanoTime();
    myPrint(log, ", Query: "+(t2-t1)/queryRepetitions/1000);
    // -------------------------------------- Comparing results
    int count=0;
    t1=System.nanoTime();
    for(int j=0; j<queryRepetitions; j++)
      count = naiveMatching(t, x1, x2, y1, y2);
    t2=System.nanoTime();
    myPrint(log, "; Naive:"+(t2-t1)/queryRepetitions/1000);
    myPrint(log, " ,  "+res.length+" points found");

    if (res.length<10)
      for (i=0; i<res.length; i++) {
        myPrint(log, "     "+res[i]);
      }
    assertTrue(count==res.length);   
  }
  // ------------------------------------------------------------
  static void myPrint(boolean really, String s) {
    if (!really) return;
    System.out.print(s);
  }
  // ------------------------------------------------------------
  public static Point[] rndPointSet(Random r, int n, int maxCoord) {
    Point[] t = new Point[n];
    Point p;
    HashSet<Point> h = new HashSet<Point>();
    Iterator<Point> itr;
    while(h.size()<n) {
      p = new Point(r.nextInt(maxCoord), r.nextInt(maxCoord));
      h.add(p);
    }
    itr=h.iterator();
    for (int i=0; i<n; i++) {
      p = (Point)(itr.next());
      t[i] = p;
    }
    return t;
  }
}
