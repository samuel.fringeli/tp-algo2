package s26;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.awt.*;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;

public class ClusteringDemoFX extends Application {

    public static final double WIDTH  = 500;
    public static final double HEIGHT = 300;

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Input data
        List<Point> pts = ClusteringDemoSwingOLD.onePointsSet();
        int nbClusters = 3;

        ClusteringChart inputChart = new ClusteringChart("Input Data", pts, null);

        Scene scene = new Scene(inputChart);
        primaryStage.setScene(scene);
        primaryStage.setMinWidth(WIDTH);
        primaryStage.setMinHeight(HEIGHT);
        primaryStage.setX(250);
        primaryStage.setY(0);
        primaryStage.setOnCloseRequest(e->{System.exit(0);});
        primaryStage.show();

        List<Cluster> clusters;

        // K-Means
        Stage kMeansStage = new Stage();
        clusters = Clustering.initKMeans(pts,nbClusters);
        Clustering.kMeans(clusters,pts);
        ClusteringChart kMeansChart = new ClusteringChart("K-Means",pts,clusters);

        Scene scene2 = new Scene(kMeansChart);
        kMeansStage.setScene(scene2);
        kMeansStage.setMinWidth(WIDTH);
        kMeansStage.setMinHeight(HEIGHT);
        kMeansStage.setX(0);
        kMeansStage.setY(450);
        kMeansStage.setOnCloseRequest(e->{System.exit(0);});
        kMeansStage.show();

        // Hierarchical
        Stage hierarchicalStage = new Stage();
        clusters = Clustering.initHierarchical(pts);
        Clustering.hierarchical(clusters,nbClusters);
        ClusteringChart hierarchicalChart = new ClusteringChart("Hierarchical",pts,clusters);

        Scene scene3 = new Scene(hierarchicalChart);
        hierarchicalStage.setScene(scene3);
        hierarchicalStage.setMinWidth(WIDTH);
        hierarchicalStage.setMinHeight(HEIGHT);
        hierarchicalStage.setX(550);
        hierarchicalStage.setY(450);
        hierarchicalStage.setOnCloseRequest(e->{System.exit(0);});
        hierarchicalStage.show();
    }

    static List<Point> onePointsSet() {
      Random r = new Random();
      ArrayList<Point> pts = new ArrayList<Point>();
      pts.addAll(pointsWithin(20, 100,  120, 200,  30, r));
      pts.addAll(pointsWithin(200, 300, 120, 200,  45, r));
      pts.addAll(pointsWithin(80, 220, 30, 80,  70, r));
      return pts;
    }

    static List<Point> pointsWithin(int xMin, int xMax,
        int yMin, int yMax, int nbOfPoints, Random r) {
      HashSet<Point> res=new HashSet<Point>();
      while(res.size()<nbOfPoints) {
        int x= xMin+4*r.nextInt((xMax-xMin)/4);
        int y= yMin+4*r.nextInt((yMax-yMin)/4);
        res.add(new Point(x,y));
      }
      ArrayList<Point> res1=new ArrayList<Point>(res);
      return res1;
    }


    public static void main(String[] args) {
        launch(args);
    }

    //=================================================================

    public static class ClusteringChart extends Pane {

      private final NumberAxis xAxis = new NumberAxis();
      private final NumberAxis yAxis = new NumberAxis();
//      private final Color[] COLORS = {
//              Color.BLUE,  Color.ORANGE, Color.RED, Color.CYAN,
//              Color.BLACK, Color.AQUA,   Color.GREEN
//      };

      private String title;
      private List<Point> points;
      private List<Cluster> clusters;
      private XYChart<Number, Number> chart;


      public ClusteringChart(String title, List<Point> points, List<Cluster> clusters) {
          this.title = title;
          this.points = points;
          this.clusters = clusters;

          chart = new ScatterChart<>(xAxis, yAxis);
          xAxis.setLabel("x value");
          yAxis.setLabel("y value");
          chart.setTitle(this.title);

          setupChart();
          getChildren().add(chart);
      }

      private void setupChart() {
          if (clusters == null) {
              XYChart.Series<Number,Number> series = new XYChart.Series<>();
              for (Point point : points) {
                  series.getData().add(new XYChart.Data<>(point.x, point.y));
              }
              chart.getData().add(series);
          } else {
              for (int i = 0; i < clusters.size(); i++) {
                  XYChart.Series<Number,Number> series = new XYChart.Series<>();
                  series.setName("cluster " + i);
                  Cluster cluster = clusters.get(i);
                  for (Point point : cluster.pts) {
                      series.getData().add(new XYChart.Data<>(point.x, point.y));
                  }
                  chart.getData().add(series);
              }
          }
      }
  }

}
