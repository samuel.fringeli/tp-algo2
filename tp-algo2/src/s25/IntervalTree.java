package s25;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class IntervalTree {
  //============================================================
  protected static class IntervTreeElt {
    public final Interval[] leftSorted;
    public final Interval[] rightSorted;
    public final int xMid;

    public IntervTreeElt (List<Interval> p, int midValue) {
      this.xMid = midValue;
      this.leftSorted=this.rightSorted=null;
      // TODO - A COMPLETER
    }

    public int midPoint() { return xMid; }

    public List<Interval> intersecting(int x) {
      assert (rightSorted.length == leftSorted.length);
      return null; // TODO - A COMPLETER
    }
    
    @Override public String toString() {
      return xMid+"("+leftSorted.length+")";
    }
  }
  //============================================================
  //============================================================
  protected final BTree<IntervTreeElt> tree;

  public IntervalTree(Interval[] p) {
    tree = fromIntervals(p);
  }

  public Interval[] containing(int x) {
    List<Interval> res = intersecting(tree.root(), x);
    return res.toArray(new Interval[0]);
  }
  
  @Override public String toString() {
    return tree.toReadableString(); 
  }
  // ----------------------------------------------------------------------
  // --- Non public methods
  // ----------------------------------------------------------------------
  private static List<Interval> intersecting(BTreeItr<IntervTreeElt> r, int x) {
    return null; // TODO - A COMPLETER
  }
  private static BTree<IntervTreeElt> fromIntervals(Interval[] p) {
    BTree<IntervTreeElt> tree = new BTree<IntervTreeElt>();
    if(p.length == 0) return tree;
    List<Integer> xt = new ArrayList<Integer>();
    for(Interval it:p) {
      xt.add(it.start); 
      xt.add(it.end);
    }
    Collections.sort(xt);
    int xMid = xt.get(p.length);  // so it's the median (of 2n values)
    List<Interval> midPart   = new ArrayList<Interval>();
    List<Interval> leftPart  = new ArrayList<Interval>();
    List<Interval> rightPart = new ArrayList<Interval>();
    return null; // TODO - A COMPLETER
  } 
}