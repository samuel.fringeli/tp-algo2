package s07;

import java.util.Arrays;

public class s12e11 {
    public static class Point {
        public float x, y;

        public Point(float x, float y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "x : " + this.x + " ; y : " + this.y;
        }
    }
    // inspired from https://stackoverflow.com/questions/52248639/
    public static Point minY(Point[] v) {
        // check for min at origin
        if (v[1].y > v[0].y && v[v.length-1].y > v[0].y) {
            return v[0];
        }

        int left = 0;
        int right = v.length-1;
        Point minY = null;
        while(left <= right) {
            int mid = left + (right-left)/2;
            if (v[(mid+1)%v.length].y > v[mid].y && v[(mid+1)%v.length].x > v[mid].x) {
                if(v[mid].y < v[mid-1].y) {
                    minY = v[mid];
                    break;
                }
                right = mid - 1;
            }
            else {
                left = mid + 1;
            }
        }
        return minY;
    }

    public static void main(String[] args) {
        Point[][] tests = {
                {new Point(0, 0), new Point(9, 5), new Point(10, 10)},
                {new Point(0, 1), new Point(10, 10), new Point(5, 8)},
                {new Point(0, 5), new Point(9, 0), new Point(10, 10)},
                {new Point(0, 5), new Point(6,2), new Point(10, 6), new Point(5,10)}};

        for(Point[] coords : tests)
            System.out.println(minY(coords) + " : " + Arrays.toString(coords));

         /* output :
            x : 0.0 ; y : 0.0 : [x : 0.0 ; y : 0.0, x : 9.0 ; y : 5.0, x : 10.0 ; y : 10.0]
            x : 0.0 ; y : 1.0 : [x : 0.0 ; y : 1.0, x : 10.0 ; y : 10.0, x : 5.0 ; y : 8.0]
            x : 9.0 ; y : 0.0 : [x : 0.0 ; y : 5.0, x : 9.0 ; y : 0.0, x : 10.0 ; y : 10.0]
            x : 6.0 ; y : 2.0 : [x : 0.0 ; y : 5.0, x : 6.0 ; y : 2.0, x : 10.0 ; y : 6.0, x : 5.0 ; y : 10.0]
        */
    }
}
