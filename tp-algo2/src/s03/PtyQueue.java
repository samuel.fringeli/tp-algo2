package s03;

public class PtyQueue<E, P extends Comparable<P>> {
  private final Heap<HeapElt> heap;
  // ------------------------------------------------------------
  public PtyQueue() {
    heap = new Heap<HeapElt>();
  }
  // ------------------------------------------------------------
  public boolean isEmpty() {
    return heap.isEmpty();
  }

  /** Adds an element elt with priority pty */
  public void enqueue(E elt, P pty) {
    heap.add(new HeapElt(pty, elt));
  }

  /** Returns the element with highest priority. PRE: !isEmpty() */
  public E consult() {
    assert (!isEmpty());
    return heap.min().theElt;
  }

  /** Returns the priority of the element with highest priority.
   *  PRE: !isEmpty() */
  public P consultPty() {
    assert(!isEmpty());
    return heap.min().thePty;
  }

  /** Removes and returns the element with highest priority.
   *  PRE: !isEmpty() */
  public E dequeue() {
    return heap.removeMin().theElt;
  }

  @Override public String toString() {
    return heap.toString();
  }
  //=============================================================
  class HeapElt implements Comparable<HeapElt> {
    private P thePty;
    private E theElt;

    public HeapElt(P thePty, E theElt) {
      this.thePty=thePty;
      this.theElt=theElt;
    }
    @Override public int compareTo(HeapElt arg0) {
      return thePty.compareTo(arg0.thePty);
    }
  }
}
