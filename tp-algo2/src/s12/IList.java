package s12;

public interface IList<E> {
  boolean isEmpty();
  int size();
  IListItr<E> iterator();
}