package s09;

import java.util.Random;
// ------------------------------------------------------------
public class StringSearching {
  // ------------------------------------------------------------
  static /*final*/ int HASHER = 301; // Maybe also try with 7 and 46237
  static /*final*/ int BASE   = 256; // Please also try with 257
  // ---------------------
  static int firstFootprint(String s, int len) {
    int result = 0;
    for (int i = 0; i < len; i++)
      result = ((BASE * result + s.charAt(i)) % HASHER);
    return result;
  }
  // ---------------------
  // must absolutely be O(1)
  // coef is (BASE  power  P.LENGTH-1)  mod  HASHER
  static int nextFootprint(int previousFootprint, char dropChar, char newChar, int coef) {
    int h = previousFootprint;
    h = (h + HASHER - (coef * dropChar % HASHER)) % HASHER; // dropChar
    h *= BASE; // shift
    return (h + newChar) % HASHER; // newChar
  }
  // ---------------------
  // Rabin-Karp algorithm
  public static int indexOf_rk(String t, String p) {
    int pHash = firstFootprint(p, p.length());
    int tHash = firstFootprint(t, p.length());
    int coef = 1;
    for (int i = 1; i < p.length(); i++) {
      coef = (BASE * coef) % HASHER;
    }
    if (pHash == tHash && p.equals(t.substring(0, p.length())))
      return 0;

    for (int i = p.length(); i < t.length(); i++) {
      tHash = nextFootprint(tHash, t.charAt(i - p.length()), t.charAt(i), coef);
      if (pHash == tHash && p.equals(t.substring(i - p.length() + 1, i + 1)))
        return i - p.length() + 1;
    }

    return -1;
  }
}
