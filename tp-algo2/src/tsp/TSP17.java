package tsp;

//======================================================================
public class TSP17 implements TSP {
  public void salesman(TSPPoint [] t, int [] path) {
    int i, j, k;
    int n = t.length;
    int thisPt, closestPt = 0;
    double shortestDist;
    double distance;
    double thisPtx, thisPty, jPtx, jPty, a, b, l;
    k= n-1;
    int sum = (n-2)*(n-1)/ 2;

    class TSPPointCustom {
      int index;
      double x;
      double y;
      TSPPointCustom(TSPPoint point, int i) {
        this.index = i;
        this.x = point.x;
        this.y = point.y;
      }
    }

    TSPPointCustom[] newPoints = new TSPPointCustom[t.length];

    for (int z = 0; z < t.length; z++) {
      newPoints[z] = new TSPPointCustom(t[z],z);
    }

    thisPt = k;
    if(thisPt < 0) return;
    path[0] = k;  // chose the starting city
    double tempDouble = 0;
    int tempInt = 0;
    for(i = 1; i < k; i++) {
      shortestDist = 0x1.fffffffffffffP+1023;
      thisPtx = newPoints[thisPt].x;
      thisPty = newPoints[thisPt].y;
      tempDouble = k - i;
      for(j = 0; j < tempDouble; j+=2) {

        jPtx = newPoints[j].x;
        jPty = newPoints[j].y;
        a = (thisPtx-jPtx);
        l = a * a;
        if (l < shortestDist) {
          b = (thisPty-jPty);
          distance = l + b * b;
          if (distance < shortestDist ){
            shortestDist = distance;
            closestPt = j;
          }
        }

        tempInt = j + 1;

        if(tempInt < tempDouble) {
          jPtx = newPoints[tempInt].x;
          jPty = newPoints[tempInt].y;
          a = (thisPtx-jPtx);
          l = a * a;
          if (l > shortestDist) continue;
          b = (thisPty-jPty);
          distance = l + b * b;
          if (distance < shortestDist ){
            shortestDist = distance;
            closestPt = tempInt;
          }
        }
      }

      sum -= newPoints[closestPt].index;
      path[i] = newPoints[closestPt].index;

      TSPPointCustom tempPoint = newPoints[closestPt];

      newPoints[closestPt] = newPoints[k - i];
      newPoints[k - i] = tempPoint;

      thisPt = k-i;
    }
    path[k] = sum;
  }
}
