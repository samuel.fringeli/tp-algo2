package s22;
import java.awt.Point;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import org.junit.Test;
import static org.junit.Assert.*;
//============================================================
public class ClosestPairTestJU {
  // ------------------------------------------------------------
  @Test public void testPerf() {
    int nbOfPoints    = 500;
    int nbOfDataSets  = 5;
    int nbOfCalls     = 100;
    testClosestPair(1*nbOfPoints, nbOfDataSets, nbOfCalls, true);
    testClosestPair(1*nbOfPoints, nbOfDataSets, nbOfCalls, false);
    testClosestPair(2*nbOfPoints, nbOfDataSets, nbOfCalls, true);
    testClosestPair(2*nbOfPoints, nbOfDataSets, nbOfCalls, false);
    testClosestPair(3*nbOfPoints, nbOfDataSets, nbOfCalls, true);
    testClosestPair(3*nbOfPoints, nbOfDataSets, nbOfCalls, false);
    testClosestPair(4*nbOfPoints, nbOfDataSets, nbOfCalls, true);
    testClosestPair(4*nbOfPoints, nbOfDataSets, nbOfCalls, false);
    testClosestPair(5*nbOfPoints, nbOfDataSets, nbOfCalls, true);
    testClosestPair(5*nbOfPoints, nbOfDataSets, nbOfCalls, false);
    testClosestPair(6*nbOfPoints, nbOfDataSets, nbOfCalls, true);
    testClosestPair(6*nbOfPoints, nbOfDataSets, nbOfCalls, false);
  }

  @Test public void testVerticallyAlignedCollection() {
    testClosestPair(500, 5, 1, false);
  }

  @Test public void testRandom() {
    testClosestPair(500, 5, 1, true);
  }
  // ------------------------------------------------------------
  public static Point[] naiveClosest(Point [] points) {
    Point[] res = new Point[2];
    double minDist = Double.POSITIVE_INFINITY;

    for (int i = 0; i < points.length; i++) {
      for (int j = 0; j < points.length; j++) {
        if (i == j) continue;
        double crtDist = points[i].distance(points[j]);
        if (crtDist < minDist) {
          minDist = crtDist;
          res[0] = points[i];
          res[1] = points[j];
        }
      }
    }
    return res;
  }
  // ------------------------------------------------------------
  public static void testClosestPair(int nbOfPoints,
                                     int nbOfDataSets,
                                     int nbOfCalls,
                                     boolean randomPoints) {
    Random r=new Random();
    long t1,t2, ds=0, dn=0;
    Point [] res=null;
    for(int i=0; i<nbOfDataSets; i++) {
      Point [] t;
      if (randomPoints) t= rndPointSet(r, nbOfPoints, 100000);
      else              t=badPoints(nbOfPoints);
      t1 = System.nanoTime();
      for(int j=0; j<nbOfCalls; j++)
        res = ClosestPair.closestPair(t);
      t2 = System.nanoTime();
      ds+=(t2-t1);
      long dsquare = (res[1].x-res[0].x)*(res[1].x-res[0].x);
      dsquare +=     (res[1].y-res[0].y)*(res[1].y-res[0].y);
      t1 = System.nanoTime();
      for(int j=0; j<nbOfCalls; j++)
        res = naiveClosest(t);
      t2 = System.nanoTime();
      dn+=(t2-t1);
      long dsquare1 = (res[1].x-res[0].x)*(res[1].x-res[0].x);
      dsquare1 +=     (res[1].y-res[0].y)*(res[1].y-res[0].y);
      assertTrue(dsquare==dsquare1);
    }
    if (nbOfCalls<=1) return;
    System.out.print("For "+nbOfPoints+(randomPoints?" random":" aligned"));
    System.out.print(" points, average time [us]: ");
    System.out.print("Sweeping: "+ (ds/nbOfDataSets/nbOfCalls/1000));
    System.out.println(" , Naive: "+ (dn/nbOfDataSets/nbOfCalls/1000));
  }
  // ------------------------------------------------------------
  public static Point[] rndPointSet(Random r, int n, int maxCoord) {
    Point[] t = new Point[n];
    Point p;
    HashSet<Point> h = new HashSet<>();
    Iterator<Point> itr;
    while(h.size()<n) {
      p = new Point(r.nextInt(maxCoord), r.nextInt(maxCoord));
      h.add(p);
    }
    itr=h.iterator();
    for (int i=0; i<n; i++) {
      p = (Point)(itr.next());
      t[i] = p;
    }
    return t;
  }
  // ------------------------------------------------------------
  static Point [] badPoints(int nbOfPoints) {
    Point[] res = new Point[nbOfPoints];
    for(int i=0; i<nbOfPoints; i++)
      res[i]=new Point(0, 10*i);
    return res;
  }
}
