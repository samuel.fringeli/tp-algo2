package s12;

public interface IMemoryManager<E> {
  int NIL = -1;
  
  int  allocate();
  void deallocate(int i);
  void setNext(int i, int v);
  void setData(int i, E   v);
  E   data(int i);
  int next(int i);
}