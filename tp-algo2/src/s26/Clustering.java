package s26;
import java.awt.Point;
import java.util.List;
import java.util.Random;

import java.util.ArrayList;
import java.util.Arrays;
//======================================================================
@SuppressWarnings("serial")
class Cluster extends Point {
  List<Point> pts;
  // ----------------------------------------
  @Override public String toString() {
    return "["+pts.size() +" points, centered on "+x+" "+y+"]";
  }
  // ----------------------------------------
  public Cluster(Cluster a, Cluster b) {
    this(a.pts);pts.addAll(b.pts);
    recomputeCenter();
  }
  // ----------------------------------------
  public Cluster(List<Point> points) {
    pts = new ArrayList<Point>(points);
    recomputeCenter();
  }
  // ----------------------------------------
  public void addPoint(Point p) {
    pts.add(p);
  }
  // ----------------------------------------
  public void removeAllMembers() {
    pts = new ArrayList<Point>();
  }
  // ----------------------------------------
  // RETURNS: whether the coordinates have changed
  //          since the last call of the method
  public boolean recomputeCenter() {
    int oldX=x, oldY=y;
    x=0; y=0;
    int n = pts.size();
    for (Point p:pts) {
      x += p.x; y += p.y;
    }
    if (n!=0) {x/=n; y/=n;}
    return (oldX!=x || oldY!=y);
  }
}
// ======================================================================
public class Clustering {
  static Random r = new Random();
  // ----------------------------------------------------------------------
  static void rndPermutation(int [] t) {
    int aux, j;
    for (int i=1; i<t.length; i++) {
      j = r.nextInt(i+1);
      aux = t[i]; t[i]=t[j]; t[j]=aux;
    }
  }
  // ----------------------------------------------------------------------
  public static List<Cluster> initKMeans(List<Point> pts, int nbOfClusters) {
    int n=pts.size();
    assert(nbOfClusters<n);
    List<Cluster> clusters = new ArrayList<Cluster>();
    int t[] = new int [n];
    for (int i=0; i<n; i++) t[i]=i;
    rndPermutation(t);
    for (int i=0; i<nbOfClusters; i++) {
      clusters.add(new Cluster(Arrays.asList(pts.get(t[i%n]))));
    }
    return clusters;
  }
  // ----------------------------------------------------------------------
  private static Cluster closestCluster(List<Cluster> clusters, Point p) {
    double closestDist=Double.POSITIVE_INFINITY;
    Cluster closest=null;
    for(Cluster c:clusters) {
      double d = p.distance(c);
      if  (d<closestDist) { closestDist=d; closest=c; }
    }
    return closest;
  }
  // ----------------------------------------------------------------------
  public static void kMeans(List<Cluster> clusters, List<Point> pts) {
    boolean stable = true;
    do {
      stable = true;
      for (Cluster c : clusters) c.removeAllMembers();
      for (Point p : pts) closestCluster(clusters, p).addPoint(p);
      for (Cluster c : clusters) stable &= !c.recomputeCenter();
    } while (!stable);
  }
  // ----------------------------------------------------------------------
  public static List<Cluster> initHierarchical(List<Point> pts) {
    List<Cluster> clusters = new ArrayList<Cluster>();
    for (int i = 0; i < pts.size(); i++) clusters.add(new Cluster(pts.subList(i, i+1)));
    return clusters;
  }
  // ----------------------------------------------------------------------
  public static void hierarchical(List<Cluster> clusters, int nbOfClusters) {
    while (clusters.size() > nbOfClusters && clusters.size() > 1) {
      int closest1 = -1;
      int closest2 = -1;
      double closestDist = Double.POSITIVE_INFINITY;
      for (int c1 = 0; c1 < clusters.size(); c1++) {
        for (int c2 = c1+1; c2 < clusters.size(); c2++) {
          double d = clusters.get(c1).distance(clusters.get(c2));
          if (d<closestDist) { closestDist=d; closest1=c1; closest2=c2; }
        }
      }
      clusters.set(closest1, new Cluster(clusters.get(closest1), clusters.get(closest2)));
      clusters.remove(closest2);
    }
  }
}

