package s25;
import java.util.Random;
import java.util.Vector;
import org.junit.Test;
import static org.junit.Assert.*;

// ============================================================
public class IntervalTreeTestJU {
  // ----------------------------------------------------------------------
  @Test
  public void testIntervalTree() {
    int query=500;
    int nbOfRepetitions=1;
    Random r = new Random();
    for(int i=0; i<1000; i++) {
      int n=r.nextInt(100);
      query=r.nextInt(104)-2;
      testIntervalTree(n, query, nbOfRepetitions, false);
    }
  }
  // ----------------------------------------------------------------------
  @Test
  public void testIntervalTreePerf() {
    int n=500000;
    int query=500;
    int nbOfRepetitions=1000;
    testIntervalTree(n, query, nbOfRepetitions, true);
    testIntervalTree(2*n, query, nbOfRepetitions, true);
    testIntervalTree(4*n, query, nbOfRepetitions, true);
    testIntervalTree(8*n, query, nbOfRepetitions, true);
  }
  // ----------------------------------------------------------------------
  private static Interval [] findNaively(Interval [] intervs, int query) {
    Vector<Interval> v = new Vector<Interval>();
    for (int i=0; i<intervs.length; i++) {
      Interval p = intervs[i];
      if (p.start <= query && p.end >= query) v.add(p);
    }
    return (Interval [])(v.toArray(new Interval[0]));
  }
  // ----------------------------------------------------------------------
  private static void p(boolean really, String s) {
    if (!really) return;
    System.out.print(s); System.out.flush();
  }
  // ----------------------------------------------------------------------
  public static void testIntervalTree(int n, int query, int nbOfRepetitions,
                                      boolean log) {
    Interval [] res=null, res2=null;
    long t1=System.currentTimeMillis();
    Random r = new Random();
    Interval [] p = new Interval [n];
    for (int i=0; i<n; i++) {
      int j = r.nextInt(n);
      p[i] = new Interval(j, j+r.nextInt(n-j));
    }
    p(log, "n= " + n +",  query x="+query+"\n"); 
    
    t1=System.currentTimeMillis();
    IntervalTree itv = new IntervalTree(p);
    p(log, "  IntervalTree Building: " + (System.currentTimeMillis()-t1)+" [ms]");
    //p(itv.toString());
    t1=System.nanoTime();
    for (int j=0; j<nbOfRepetitions; j++) {
      res = itv.containing(query);
    }
    p(log, ", \tQuery: " + (System.nanoTime()-t1)/1000/nbOfRepetitions+" [us]");
    
    t1=System.nanoTime();
    for (int j=0; j<nbOfRepetitions; j++) {
      res2 = findNaively(p, query);
    }
    p(log, "; \tNaive: " + (System.nanoTime()-t1)/1000/nbOfRepetitions+" [us]");

    p(log,"; \tfound: "+res.length+" intervals \n");
    if (res.length<10) 
      for (int i=0; i<res.length; i++) 
        p(log, ""+res[i]);
    p(log, "");
    
    assertTrue(res.length == res2.length);
  }
}

// ============================================================
