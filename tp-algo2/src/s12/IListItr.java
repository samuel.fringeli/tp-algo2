package s12;

public interface IListItr<E> {
  void insertAfter(E e);
  void removeAfter();
  E   consultAfter();
  void goToNext();
  void goToPrev();
  void goToFirst();
  void goToLast();
  boolean isFirst();
  boolean isLast();
}