package s05;

public class s12e02 {
  private static void rotate(int[] t, int number) {
    int length = t.length;
    int[] t_temp = new int[t.length];
    System.arraycopy(t,0, t_temp,length-number,number);
    System.arraycopy(t,number,t_temp,0,length-number);
    System.arraycopy(t_temp,0,t,0, length);
  }

  private static void printArray(int[] a) {
    System.out.print("[ ");
    for (int i = 0; i < a.length; i++) {
      System.out.print(a[i] + " ");
    }
    System.out.println("]");
  }

  public static void main(String[] args) {
    //int[] t={0,1,2,3,4,5,6};
    // rotate(t,3);
    int[] t = {0,1,2,3,4,5,6,7,8,9,10,11,12,13};
    rotate(t,5);
    printArray(t);

  }
}
