package s06;
import s01.BTree;
import s01.BTree.*;
import s01.BTreeItr;

public class s12e12 {
    private static BTNode commonAncestor(BTNode a, BTNode b) {
        int aDepth = calculateDepth(a);
        int bDepth = calculateDepth(b);
        // A ce stade dans le pire des cas (les deux noeuds sont des racines)
        // on a une complexité de O(2hauteur) <=> O(hauteur) en CPU
        if (aDepth < bDepth) {
            for (int i = 0; i < bDepth - aDepth; i++) {
                b = b.parent;
            }
        }else {
            for (int i = 0; i < aDepth - bDepth; i++) {
                a = a.parent;
            }
        }
        // A ce stade dans le pire des cas (un des noeuds est une
        // feuilles et l'autre et la racine)
        // on a une complexité de O(3hauteur) <=> O(hauteur) en CPU

        BTNode aAncestor = a.parent;
        BTNode bAncestor = b.parent;

        while (aAncestor != bAncestor) {
            aAncestor = aAncestor.parent;
            bAncestor = bAncestor.parent;
        }

        // A ce stade dans le pire des cas (l'ancetre commun est la
        // racine et les deux noeuds sont des feuilles)
        // on a une complexité de O(4hauteur) <=> O(hauteur) en CPU
        return aAncestor;

        // On a donc une compléxité de O(hauteur) en CPU et O(1) en RAM
        // car le nombre d'éléments stockés est constants.
    }

    // Pire des cas complexité en CPU O(hauteur)
    private static int calculateDepth(BTNode a) {
        int depth = 0;
        while (a.parent != null) {
            a = a.parent;
            depth++;
        }
        return depth;
    };

    private static void createSampleTree(BTreeItr bTreeItr) {
        bTreeItr.insert(1);

        bTreeItr.left().insert(2);
        bTreeItr.left().left().insert(4);
        bTreeItr.left().right().insert(5);
        bTreeItr.left().right().left().insert(8);
        bTreeItr.left().right().right().insert(9);

        bTreeItr.right().insert(3);
        bTreeItr.right().left().insert(6);
        bTreeItr.right().left().left().insert(10);
        bTreeItr.right().left().left().left().insert(12);
        bTreeItr.right().left().left().right().insert(13);
        bTreeItr.right().left().right().insert(11);
        bTreeItr.right().right().insert(7);
    }

    public static void main(String[] args) {
        BTree bTree = new BTree();
        BTreeItr bTreeItr = new BTreeItr(bTree);
        createSampleTree(bTreeItr);
        System.out.println(bTree.toReadableString());
        BTNode firstNode = bTree.root().left().left().down;
        //BTNode secondNode = bTree.root().left().right().right().down;
        BTNode secondNode = bTree.root().right().left().left().left().down;
        BTNode commonAncestor = commonAncestor(firstNode, secondNode);
        System.out.println("Done");
    }

}
