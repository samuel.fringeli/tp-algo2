package s05;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class s12e05 {
  static class AnagramDico {
    private Map<String, ArrayList<String>> map = new HashMap<>();

    private String orderString(String word) {
      char[] letters = word.toCharArray();
      Arrays.sort(letters);
      return new String(letters);
    }

    AnagramDico(String[] anagrams) {
      for (String anagram: anagrams) {
        String sorted = orderString(anagram);
        map.computeIfAbsent(sorted, k -> new ArrayList<>());
        map.get(sorted).add(anagram);
      }
    }

    void printAnagram(String word) {
      map.get(orderString(word)).forEach(e -> System.out.print(e + " "));
    }
  }

  public static void main(String[] args) {
    new AnagramDico(new String[] {"spot", "zut", "pots", "stop", "hello"}).printAnagram("pots");
  }
}
