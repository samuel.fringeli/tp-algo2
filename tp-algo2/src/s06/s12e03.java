package s06;

import java.util.Stack;

public class s12e03 {
  static class Element {
    float elt, min, max;

    Element(float elt, float min, float max) {
      this.elt = elt;
      this.min = min;
      this.max = max;
    }
  }

  static class BetterStack extends Stack<Element> {
    void push(float e) {
      super.push(new Element(e, // elt
        isEmpty() ? e:Math.min(e, this.peek().min), // min
        isEmpty() ? e:Math.max(e, this.peek().max)  // max
      ));
    }

    float pop(float e) {
      return super.pop().elt;
    }

    float consultMin() {
      return super.peek().min;
    }

    float consultMax() {
      return super.peek().max;
    }
  }

  public static void main(String[] args) {
    BetterStack betterStack = new BetterStack();
    betterStack.push(4);
    betterStack.push(7);
    betterStack.push(2);
    betterStack.push(9);
    betterStack.push(5);
    System.out.println(betterStack.consultMin());
  }
}
