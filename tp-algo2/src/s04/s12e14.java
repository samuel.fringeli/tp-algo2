package s04;

import java.util.Random;

public class s12e14 {
  public static void test2d() {
    int[][] matrix = {{1,3,2,4},{2,8,1,3},{2,5,3,8},{4,4,4,4}};
    int searchedNumber = 8;

    int oldRow = -1;
    int oldCol = -1;

    int row = -1;
    int col = -1;

    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[0].length; j++) {
        if (matrix[i][j] == searchedNumber) {
          row = i;
          col = j;
          for (int k = row -1; k >= 0; k--) {
            if (matrix[k][j] == searchedNumber && k != oldRow) break;
            matrix[k][j] = searchedNumber;
          }
          for (int k = col -1; k >= 0; k--) {
            if (matrix[i][k] == searchedNumber && k != oldCol) break;
            matrix[i][k] = searchedNumber;
          }
          oldRow = row;
          oldCol = col;
        }

        if (row != -1 && col != -1) {
          if (i == row || j == col) {
            matrix[i][j] = searchedNumber;
          }
        }
      }
    }
    for (int i = 0; i < matrix.length; i++) {
      for(int j = 0; j < matrix[0].length; j++)
      {
        System.out.printf("%5d ", matrix[i][j]);
      }
      System.out.println();
    }

  }

  public static void test3d() {
    Random random = new Random();

    for (int a = 10; a <= 100; a++) {
      System.out.print("testing for ");
      System.out.println(a);

      int[][][] matrix = new int[a][a][a];
      int[][][] matrixcheck = new int[a][a][a];

      for(int i = 0; i < matrix.length; i++) {
        for(int j = 0; j < matrix[0].length; j++) {
          for(int k = 0; k < matrix[0][0].length; k++) {
            matrixcheck[i][j][k] = 0;
            matrix[i][j][k] = random.nextInt();
          }
        }
      }

      int searchedNumber = random.nextInt();

      int oldRow = -1;
      int oldCol = -1;
      int oldDepth = -1;

      int row = -1;
      int col = -1;
      int depth = -1;

      for (int i = 0; i < matrix.length; i++) {
        for (int j = 0; j < matrix[0].length; j++) {
          for (int k = 0; k < matrix[0][0].length; k++) {
            if (matrix[i][j][k] == searchedNumber) {
              row = i;
              col = j;
              depth = k;

              for (int l = row -1; l >= 0; l--) {
                if (matrix[l][j][k] == searchedNumber && l != oldRow) break;
                matrixcheck[l][j][k]++;
                matrix[l][j][k] = searchedNumber;
              }
              for (int l = col -1; l >= 0; l--) {
                if (matrix[i][l][k] == searchedNumber && l != oldCol) break;
                matrixcheck[i][l][k]++;
                matrix[i][l][k] = searchedNumber;
              }
              for (int l = depth -1; l >= 0; l--) {
                if (matrix[i][j][l] == searchedNumber && l != oldDepth) break;
                matrixcheck[i][j][l]++;
                matrix[i][j][l] = searchedNumber;
              }
              oldRow = row;
              oldCol = col;
              oldDepth = depth;
            }

            if (row != -1 && col != -1) {
              if (i == row || j == col) {
                matrix[i][j][k] = searchedNumber;
                matrixcheck[i][j][k]++;
              }
            }
          }
        }
      }
      for(int i = 0; i < matrixcheck.length; i++) {
        for(int j = 0; j < matrixcheck[0].length; j++) {
          for(int k = 0; k < matrixcheck[0][0].length; k++) {
            if (matrixcheck[i][j][k] > 2 ) {
              System.out.println("Erreur");
            }
          }
        }
      }
    }
  }

  public static void main(String[] args) {
    test2d();
    test3d();
  }
}
