package s02;
import java.util.Random;

public class s12e18 {
    static boolean isOk(int[] t, int r) {
        int n=t.length;
        if(n==1) return r==0;
        if(r==0) return t[r] <= t[r+1];
        if(r==n-1) return t[r] <= t[r-1];
        return t[r] <= t[r+1] && t[r] <= t[r-1];
    }

    static int findR(int[] t, int minIndex, int maxIndex) {
        int middle = ((maxIndex - minIndex) / 2) + minIndex;
        if (minIndex + 1 == maxIndex)
            return t[minIndex] <= t[maxIndex] ? minIndex:maxIndex;
        if (t[middle] <= t[middle - 1] && t[middle] <= t[middle + 1])
            return middle;
        if (t[middle] <= t[middle - 1])
            return findR(t, middle, maxIndex);
        return findR(t, minIndex, middle);
    }

    static void printArr(int[] t) {
        System.out.print("[");
        for (int i = 0; i < t.length; i++) {
            System.out.print(t[i]);
            if (i + 1 != t.length)
                System.out.print(", ");
        }
        System.out.println("]");
    }

    public static void main(String args[]) {
        boolean hadError = false;
        for (int h = 0; h < 1; h++) {
            Random rd = new Random();
            int[] randomArr = new int[10];
            for (int i = 0; i < randomArr.length; i++) {
                randomArr[i] = rd.nextInt(10);
            }
            printArr(randomArr);
            boolean result = isOk(randomArr, findR(randomArr, 0, randomArr.length - 1));
            if (!result) {
                System.out.println("error !");
                hadError = true;
                break;
            }
        }
        if (!hadError)
            System.out.println("All tests were successful");
    }
}
