package s02;
import java.util.Arrays;

public class s12e15 {
    static int nbOfStars(float[] t) {
        int nbOfStars = 1;
        int currentLvl = 1;
        int browsedBeforeLastUp = 0;

        for (int i = 0; i < t.length; i++) {
            if (i + 1 == t.length || t[i] == t[i + 1]) {
                nbOfStars += currentLvl;
                browsedBeforeLastUp++;
            } else if (t[i] > t[i + 1]) { // down
                if (currentLvl == 1) {
                    nbOfStars += browsedBeforeLastUp;
                }
                else {
                    currentLvl--;
                }
                nbOfStars += currentLvl;
                browsedBeforeLastUp++;
            } else { // up
                browsedBeforeLastUp = 0;
                currentLvl++;
                nbOfStars += currentLvl;
            }
        }
        return nbOfStars;
    }

    public static void main(String args[]) {
        float testArr1[] = {4, 2, 2, 8, 8, 3, 1, 5};
        System.out.println(nbOfStars(testArr1)); // 15
        float testArr2[] = {2, 1, 9, 9, 4, 6, 1, 7, 5};
        System.out.println(nbOfStars(testArr2)); // 14
        float testArr3[] = {0, 8, 6, 1, 5, 2, 6, 3, 0, 9};
        System.out.println(nbOfStars(testArr3)); // 18
    }
}
