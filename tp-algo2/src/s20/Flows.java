package s20;
import java.util.*;

public class Flows {
  //==================================================
  public static class MaxFlowResult {
    public WeightedDiGraph flow;
    public int       throughput;
    public boolean[] isInSourceGroup; // gives the Min-Cut solution
  }
  //==================================================

  public static void suppressEmptyEdges(WeightedDiGraph g) {
    for(WeightedDiGraph.Edge e:g.allEdges())
      if (e.weight == 0) 
        g.removeEdge(e.from, e.to);
  }

  public static boolean[] isAccessibleFrom(WeightedDiGraph g, int from, boolean[] r) {
    r[from] = true;
    for (Integer i : g.neighboursFrom(from))
      if (!r[i])
        isAccessibleFrom(g, i, r);
    return r;
  }

  public static MaxFlowResult maxFlow(WeightedDiGraph capacity, int source,  int sink) {
    MaxFlowResult r = new MaxFlowResult();

    WeightedDiGraph resid = initialResid(capacity);
    r.flow  = initialFlow(capacity);

    while(true) {
      List<Integer> path = resid.pathBetween(source, sink);
      if (path == null) break;

      int benefit = minStep(resid, path);
      r.throughput += benefit;

      updateFlow(r.flow, resid, path, benefit);
      suppressEmptyEdges(resid);
    }

    suppressEmptyEdges(r.flow);
    r.isInSourceGroup = new boolean[resid.nbOfVertices()];
    isAccessibleFrom(resid, source, r.isInSourceGroup);

    return r;
  }

  public static int minStep(WeightedDiGraph g, List<Integer> path) {
    int min = Integer.MAX_VALUE;
    int last = -1;
    for (Integer step : path) {
      if (last != -1) {
        int weight = g.edgeWeight(last, step);
        if (weight < min) min = weight;
      }
      last = step;
    }
    return min;
  }

  public static WeightedDiGraph initialResid(WeightedDiGraph cap) {
    WeightedDiGraph resid = new WeightedDiGraph(cap);
    suppressEmptyEdges(resid);
    return resid;
  }

  public static WeightedDiGraph initialFlow(WeightedDiGraph cap) {
    return new WeightedDiGraph(cap.nbOfVertices());
  }

  public static void updateFlow(WeightedDiGraph flow, WeightedDiGraph resid, List<Integer> path, int benefit) {
    int last = -1;
    for (Integer step : path) {
      if (last != -1) {
        addCost(flow, last, step, benefit);
        addCost(flow, step, last, -benefit);
        addCost(resid, last, step, -benefit);
        addCost(resid, step, last, benefit);
      }
      last = step;
    }
  }

  private static void addCost(WeightedDiGraph g, int from, int to, int delta) {
    g.putEdge(from, to, (g.isEdge(from, to) ? g.edgeWeight(from, to) : 0) + delta);
  }
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  public static void main(String [] args) {
    int nVertices = 6;  //int nEdges = 12;
    final int A=0, B=1, C=2, D=3, E=4, F=5;
    int    [] srcs  = {A, A, A, B, B, D, D, D, D, E, F, F };
    int    [] dsts  = {B, C, F, F, C, A, B, C, E, A, D, E };
    int    [] costs = {12,6, 14,1, 7, 9, 3, 2, 4, 5, 10,11};
    
    WeightedDiGraph g = new WeightedDiGraph(nVertices, srcs, dsts, costs);
    System.out.println("                Input Graph: " + g);
    
    int source=0, sink=2;
    System.out.println("              Source vertex: "+ source);
    System.out.println("                Sink vertex: "+ sink);
    MaxFlowResult result = maxFlow(g, source, sink);
    System.out.println("               Maximal flow: "+ result.flow);
    System.out.println("           Total throughput: "+ result.throughput);
    System.out.println("Source-group in the min-cut: "+ groupFromArray(result.isInSourceGroup));    
  }

  static List<Integer> groupFromArray(boolean[] t) {
    List<Integer> res=new LinkedList<>();
    for(int i=0; i<t.length; i++)
      if (t[i]) res.add(i);
    return res;
  }

}
