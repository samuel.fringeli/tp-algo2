package s05;

import java.util.Stack;

public class s12e01 {

  static class Lifo<E> {
    private Stack<E> stack;

    public Lifo() {
      stack = new Stack<>();
    }

    public void push(E element) {
      stack.add(element);
    }

    public E get() {
      return stack.get(0);
    }

    public E pop() {
      E result = get();
      // this one has a O(1) complexity, because java implements that from a dynamic list
      stack.remove(0);
      return result;
    }
  }

  public static void main(String[] args) {
    Lifo<Integer> lifo = new Lifo<>();
    lifo.push(1);
    lifo.push(2);
    lifo.push(3);
    System.out.println(lifo.get()); // first : 1
    System.out.println(lifo.pop()); // first : 1
    System.out.println(lifo.pop()); // second : 2
    System.out.println(lifo.get()); // third : 3
  }
}
