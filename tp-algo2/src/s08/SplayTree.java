package s08;

public class SplayTree<K extends Comparable<K>> {
  BTree<K> tree = new BTree<K>();
  int crtSize = 0;
  // --------------------------------------------------
  public SplayTree() { super(); }
  // --------------------------------------------------
  public void    add     (K e) {
    if (contains(e)) return; // This "splays" the tree!
    BTreeItr<K> ti=tree.root();
    if (isEmpty()) {
      ti.insert(e);
      crtSize++;
      return;
    }
    crtSize++;

    boolean bigger = e.compareTo(tree.root.elt) > 0;
    if (bigger ? ti.hasRight():ti.hasLeft()) {
      BTree<K> aux = bigger ? ti.right().cut():ti.left().cut();
      BTree<K> current = ti.cut();
      ti.insert(e);
      (bigger ? ti.right():ti.left()).paste(aux);
      (bigger ? ti.left():ti.right()).paste(current);
    }
    else {
      BTree<K> current = ti.cut();
      ti.insert(e);
      (bigger ? ti.left():ti.right()).paste(current);
    }
  }
  // --------------------------------------------------
  public void    remove  (K e) {
    if (! contains(e)) return; // This "splays" the tree!
    crtSize--;
    if (tree.root().hasLeft() && tree.root().hasRight()) {
      BTree<K> oldRight = tree.root().right().cut();
      tree=tree.root().left().cut();
      BTreeItr<K> maxInLeft= tree.root().rightMost().up();
      BTreeItr<K> ti=splayToRoot(maxInLeft); // now tree has no right subtree!
      ti.right().paste(oldRight);
    } else {  // the tree has only one child
      if (tree.root().hasLeft()) tree=tree.root().left() .cut();
      else                       tree=tree.root().right().cut();
    }
  }
  // --------------------------------------------------
  public boolean contains(K e) {
    if (isEmpty()) return false;
    BTreeItr<K> ti=locate(e);
    boolean absent=ti.isBottom();
    if (absent) ti=ti.up();
    ti=splayToRoot(ti);
    return !absent;
  }
  // --------------------------------------------------
  protected  BTreeItr<K> locate(K e) {
    BTreeItr<K> ti = tree.root();
    while(!ti.isBottom()) {
      K c = ti.consult();
      if (e.compareTo(c)==0) break;
      if (e.compareTo(c)< 0) ti = ti.left();
      else                   ti = ti.right();
    }
    return ti;
  }
  // --------------------------------------------------
  public int     size()    { return crtSize;}
  // --------------------------------------------------
  public boolean isEmpty() { return size() == 0;}
  // --------------------------------------------------
  public K    minElt() {
    splayToRoot(tree.root().rightMost().up());
    return tree.root.elt;
  }
  // --------------------------------------------------
  public K    maxElt() {
    splayToRoot(tree.root().leftMost().up());
    return tree.root.elt;
  }
  // --------------------------------------------------
  public String toString() {
    return ""+tree.toReadableString()+"SIZE:"+size();
  }
  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  // PRE:     ! ti.isBottom()
  // RETURNS: root position
  // WARNING: ti is no more valid
  private BTreeItr<K> splayToRoot(BTreeItr<K> ti) {
    if (ti.isRoot()) {
      return ti;
    }
    if (ti.up().isRoot()) {
      return applyZig(ti);
    }
    while (!ti.isRoot() && !ti.up().isRoot()) {
      boolean zigZig = (ti.isLeftArc() && ti.up().isLeftArc()) || (!ti.isLeftArc() && !ti.up().isLeftArc());
      ti = zigZig ? applyZigZig(ti):applyZigZag(ti);
    }
    return splayToRoot(ti);
  }
  // --------------------------------------------------
  // PRE / RETURNS : Zig situation (see schemas)
  // WARNING: ti is no more valid
  private BTreeItr<K> applyZig(BTreeItr<K> ti) {
    boolean leftZig=ti.isLeftArc();
    ti=ti.up();
    if (leftZig) ti.rotateRight();
    else         ti.rotateLeft();
    return ti;
  }
  // --------------------------------------------------
  // PRE / RETURNS : ZigZig situation (see schemas)
  // WARNING: ti is no more valid
  private BTreeItr<K> applyZigZig(BTreeItr<K> ti) {
    boolean isLeftAux = ti.isLeftArc();
    ti = ti.up().up();
    for (int i = 0; i <= 1; i++)
      if (isLeftAux) ti.rotateRight();
      else ti.rotateLeft();
    return ti;
  }
  // --------------------------------------------------
  // PRE / RETURNS : ZigZag situation (see schemas)
  // WARNING: ti is no more valid
  private BTreeItr<K> applyZigZag(BTreeItr<K> ti) {
    return applyZig(applyZig(ti));
  }
  // --------------------------------------------------
}
