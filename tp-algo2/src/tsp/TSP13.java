package tsp;
//======================================================================
public class TSP13 implements TSP {
  public void salesman(TSPPoint [] t, int [] path) {
    int i, j;
    int n = t.length;
    boolean[] visited = new boolean[n];
    int thisPt, closestPt = 0;
    double shortestDist;
    double distance;
    double thisPtx, thisPty, jPtx, jPty;
    int sum = n*(n-1) / 2;
    int pathSum = n-1;

    thisPt = n-1;
    if(thisPt < 0) return;
    visited[thisPt] = true;
    path[0] = n-1;  // chose the starting city
    for(i=1; i<n-1; i++) {
      shortestDist = 0x1.fffffffffffffP+1023;
      thisPtx = t[thisPt].x;
      thisPty = t[thisPt].y;
      for(j=0; j<n-1; j++) {
        if (visited[j])
          continue;
        jPtx = t[j].x;
        jPty = t[j].y;
        distance = (thisPtx-jPtx) * (thisPtx-jPtx) + (thisPty-jPty) * (thisPty-jPty);
        if (distance < shortestDist ){
          shortestDist = distance;
          closestPt = j;
        }
      }
      pathSum += closestPt;
      path[i] = closestPt;
      visited[closestPt] = true;
      thisPt = closestPt;
    }
    path[n-1] = sum - pathSum;
  }
}
