package s18;
import java.util.Comparator;
import java.util.PriorityQueue;

import s18.WeightedUGraph.Edge;

public class Kruskal {

  /* RETURNS: the total weight of the Minimum Spanning Tree
   * PRE:     res has the same vertices as g, but no edge
   * POST:    res is the resulting sub graph
   */
  public static int kruskal(WeightedUGraph g, WeightedUGraph res) {
    int n = g.nbOfVertices();

    DisjointSets sets = new DisjointSets(n);

    PriorityQueue<Edge> edges = new PriorityQueue<>(Comparator.comparingInt((Edge e) -> e.weight));
    edges.addAll(g.allEdges());

    int totalWeight = 0;

    while (!sets.isUnique() && !edges.isEmpty()) {

      Edge edge = edges.poll();

      if (!sets.isInSame(edge.from, edge.to)) {
        res.putEdge(edge.from, edge.to, edge.weight);
        sets.union(edge.from, edge.to);
        totalWeight += edge.weight;
      }
    }

    return totalWeight;
  }

  // ------------------------------------------------------------
  public static void main(String [] args) {
    int nVertices = 6; // int nEdges = 12;
    final int A=0, B=1, C=2, D=3, E=4, F=5;
    int[] srcs    = {A, A, A, B, B, D, D, D, D, E, F, F };
    int[] dsts    = {B, C, F, F, C, A, B, C, E, A, D, E };
    int[] weights = {12,6, 14,1, 7, 9, 3, 2, 4, 11,10,5 };

    WeightedUGraph g = new WeightedUGraph(nVertices, srcs, dsts, weights);
    System.out.println("Input Graph: " + g);

    WeightedUGraph res = new WeightedUGraph(g.nbOfVertices());
    int  cost = kruskal(g, res);
    System.out.println("\nMinimal Spanning Tree of cost: " +cost);
    for(WeightedUGraph.Edge e:g.allEdges())
      System.out.println(e.from +" to "+e.to + " of cost " +e.weight);
  }
}
