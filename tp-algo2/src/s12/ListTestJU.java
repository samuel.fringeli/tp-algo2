package s12;
import java.util.*;
import java.util.function.Supplier;
import org.junit.Test;
import static org.junit.Assert.*;

public final class ListTestJU {
  static Random r = new Random(612);
  static boolean displayOperationLetter = false;

  @Test
  public void testXORList() {
    IMemoryManager<Integer> mm = new MemoryManager<>();
    Supplier<IList<Integer>> iListCreator = () -> new XORList<Integer>(mm);
    randomizedTest(iListCreator);
  }

  @Test
  public void testDoublyLinkedList() {
    IMemoryManager<Integer> mm = new MemoryManager<>();
    Supplier<IList<Integer>> iListCreator = () -> new XORList<Integer>(mm);
    randomizedTest(iListCreator);
  }

  public void randomizedTest(Supplier<IList<Integer>> iListCreator) {
    int nOperations = 100;
    int nRepetitions = 10;
    for(int i=0; i<nRepetitions; i++)
      test(nOperations, iListCreator);
  }


  void test(int n, Supplier<IList<Integer>> iListCreator) {
    IList<Integer> l = iListCreator.get();
    IListItr<Integer> li = l.iterator();
    ListWithArray la = new ListWithArray();

    int a,i;
    for (i=0; i<n; i++) {
      switch(r.nextInt(6)) {
      case 0: // insert
        myPrint("i");
        a = r.nextInt(100);
        li.insertAfter(a); la.insertAfter(a);
        break;
      case 1: // remove
        if (la.isLast()) break;
        myPrint("r");
        li.removeAfter(); la.removeAfter();
        break;
      case 2: // next
        if (la.isLast()) break;
        myPrint("n");
        li.goToNext(); la.goToNext();
        break;
      case 3: // prev
        if (la.isFirst()) break;
        myPrint("p");
        li.goToPrev(); la.goToPrev();
        break;
      case 4: // first
        myPrint("f");
        li.goToFirst(); la.goToFirst();
        break;
      case 5: // last
        myPrint("l");
        li.goToLast(); la.goToLast();
        break;
      }
      assertEquals(la.size(), l.size());
      assertEquals(la.isEmpty(), l.isEmpty());
      assertEquals(la.isFirst(), li.isFirst());
      assertEquals(la.isLast(), li.isLast());
      if(!li.isLast())
        assertEquals(la.consultAfter(), li.consultAfter());
    }
  }

  void myPrint(String s) {
    if(!displayOperationLetter) return;
    System.out.print(s);
  }
  //============================================================
  static final class ListWithArray {
    private ArrayList<Integer> v = new ArrayList<>();
    private int pos;
    boolean isEmpty() { return v.isEmpty();                    }
    int        size() { return v.size();                       }
    // ------------------
    void  insertAfter(int e) { v.add(pos, e);                  }
    void  removeAfter() {  v.remove(pos); if(isEmpty()) pos=0; }
    Integer  consultAfter() {  return v.get(pos);              }
    void goToNext()       { pos++;                             }
    void goToPrev()       { pos--;                             }
    void goToFirst()      { pos=0;                             }
    void goToLast()       { pos = size()==0?0:size();          }
    boolean isFirst()     { return pos==0;                     }
    boolean isLast()      { return size()==0?true:pos==size(); }
  }

}
