package s12;

public final class DoublyLinkedList<E> implements IList<E> {
  //===================================
  static class ListNode<T> {
    T elt;
    ListNode<T> next, prev;

    ListNode(T theElement, ListNode<T> thePrev, ListNode<T> theNext) {
      elt = theElement;
      next = theNext;
      prev = thePrev;
    }
  }
  //===================================

  ListNode<E> first, last;
  int size;
  final IMemoryManager<E> mm;  // unused...
  
  public DoublyLinkedList(IMemoryManager<E> mm) {
    first = last = null;
    size = 0;
    this.mm = mm;
  }
  
  @Override
  public boolean isEmpty() {
    return size == 0;
  }
  
  @Override
  public int size() {
    return size;
  }
  
  @Override
  public IListItr<E> iterator() {
    return new DoublyLinkedListItr<E>(this);
  }
  
  public String toString() {
    String s="[";
    IListItr<E> li = iterator();
    while(!li.isLast()) {
      s+=" "+li.consultAfter();
      li.goToNext();
    }
    return s+" ]";
  }
  
  // ==================================================
  // === Small demo program ===
  // ==================================================
  
  public static void main (String [] args) {
    if (findsBugInScenario1()) 
      System.out.println("Something is wrong...");
    else
      System.out.println("Demo passed successfully");
  }
  // --------------------
  private static boolean findsBugInScenario1() {
    IMemoryManager<Integer> mm = new MemoryManager<>();
    IList<Integer> l = new DoublyLinkedList<>(mm);
    IListItr<Integer> li = l.iterator();
    
    if (!l.isEmpty())  return true;
    if (!li.isFirst()) return true;
    if (!li.isLast())  return true;
    assert hasSameContents(new int[] {}, l);
    li.insertAfter(30);
    assert hasSameContents(new int[] {30}, l);
    li.insertAfter(10);
    assert hasSameContents(new int[] {10,30}, l);
    li.goToNext();
    li.insertAfter(20);
    assert hasSameContents(new int[] {10,20,30}, l);
    li.goToLast();
    li.insertAfter(50);
    assert hasSameContents(new int[] {10,20,30,50}, l);
    li.insertAfter(40);
    assert hasSameContents(new int[] {10,20,30,40,50}, l);
    li.goToLast();
    if (!li.isLast()) return true;
    li.goToFirst(); li.removeAfter();
    assert hasSameContents(new int[] {20,30,40,50}, l);
    li.goToNext();  
    assert hasSameContents(new int[] {20,30,40,50}, l);
    li.removeAfter();
    assert hasSameContents(new int[] {20,40,50}, l);
    li.goToNext();  li.removeAfter();
    assert hasSameContents(new int[] {20,40}, l);
    li.goToFirst();
    if (li.consultAfter() != 20) return true;
    li.goToNext();
    if (li.consultAfter() != 40) {
      System.out.println(li.consultAfter() + "instead of 40");
      return true;
    }
    li.goToNext();
    if (!li.isLast()) return true;

    return false;
  }
  
  static boolean hasSameContents(int[] t, IList<Integer> list) {
    IListItr<Integer> li = list.iterator();
    if(!li.isFirst()) return false;
    if(list.size() != t.length) return false;
    for (int i=0; i<t.length; i++) { 
      if (li.consultAfter() != t[i]) return false;
      if(li.isLast()) return false;
      li.goToNext();
    }
    if(!li.isLast()) return false;
    for (int i=t.length-1; i>=0; i--) { 
      li.goToPrev();
      if (li.consultAfter() != t[i]) return false;
    }
    if(!li.isFirst()) return false;
    return true;
  }
}
