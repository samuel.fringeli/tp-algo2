package s13;

import java.io.*;

// Reads any file one bit at a time (most significant bit first)
public class BitReader {
  private final FileInputStream fis;
  private int mask = 0b10000000;
  private int tmp;
  private long l; // length of file
  //--------------------
  public BitReader(String filename) throws IOException {
    File f = new File(filename);
    fis = new FileInputStream(f);
    tmp = fis.read();
    l = f.length();
  }
  //--------------------
  public void close() throws IOException {
    if (isOver()) fis.close();
  }
  //--------------------
  public boolean next() throws IOException {
    boolean flag = (mask & tmp) == mask;
    mask >>>= 1;

    if (mask == 0) {
      tmp = fis.read();
      mask = 0b10000000;
      l--;
    }
    return flag;
  }
  //--------------------
  public boolean isOver() {
    return l == 0;
  }
  //--------------------
  // Tiny demo...
  public static void main(String [] args) {
    String filename="a.txt";
    try {
      BitReader b = new BitReader(filename);
      int i=0;
      while(!b.isOver()) {
        System.out.print(b.next()?"1":"0");
        i++;
        if (i%8  == 0) System.out.print(" ");
        if (i%80 == 0) System.out.println("");
      }
      b.close();
    } catch (IOException e) {
      throw new RuntimeException(""+e);
    }
  }
}
