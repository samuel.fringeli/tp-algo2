package s12;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

// Naive "memory manager" (as in S02, but with ArrayList instead of arrays)
public final class MemoryManager<E> implements IMemoryManager<E> {

  private static final int DEFAULT_SIZE = 2;
  private final ArrayList<E>       data = new ArrayList<>();
  private final ArrayList<Integer> next = new ArrayList<>();
  private int firstFreeCell;

  public MemoryManager() {
    this(DEFAULT_SIZE);
  }
  
  public MemoryManager(int initialSize) {
    addCells(initialSize);
    firstFreeCell = 0;
  }

  private void addCells(int n) {
    for (int i=0; i<n; i++) {
      data.add(null);
      next.add(next.size()+1);
    }
    next.set(next.size()-1, NIL);
  }

  private void checkSize() {
    if (firstFreeCell != NIL) return;
    int firstFreshCell = data.size();
    // reallocating memory...
    addCells(data.size());
    firstFreeCell = firstFreshCell;
  }

  @Override
  public int allocate() {
    checkSize();
    int i = firstFreeCell;
    assert i != NIL;
    firstFreeCell = next(i);  
    setData(i, null);  // just to give a "clean" cell to the user
    setNext(i, NIL);   // just to give a "clean" cell to the user
    assert isAllocated(i);
    return i;
  }

  @Override
  public void deallocate(int i) {
    assert isAllocated(i);
    setNext(i, firstFreeCell);
    setData(i, null);
    firstFreeCell = i;
  }
  
  @Override
  public void setNext(int i, int v) {
    assert isAllocated(i);
    next.set(i, v);
  }
  
  @Override
  public void setData(int i, E v) {
    data.set(i,  v);
  }
  
  @Override
  public E data(int i) {
    return data.get(i);
  }
  
  @Override
  public int next(int i) {
    return next.get(i);
  }
  
  boolean isAllocated(int i) {
    Set<Integer> unAlloc = unAllocated();
    return !unAlloc.contains(i);
  }
  
  Set<Integer> unAllocated() {
    Set<Integer> s = new HashSet<>();
    int j=firstFreeCell;
    while(j != NIL) {
      s.add(j);
      j=next(j);
    }
    return s;
  }
  
  @Override public String toString() {
    String s=" ";
    for(int i=0; i<data.size(); i++) {
      s += i+", ";
    }
    s += '\n';
    s += next;
    s += "\n  firstFreeCell="+firstFreeCell;
    return s;
  }
}