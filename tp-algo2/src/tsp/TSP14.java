package tsp;
//======================================================================
public class TSP14 implements TSP {
  public void salesman(TSPPoint [] t, int [] path) {
    int i, j, k;
    int n = t.length;
    boolean[] visited = new boolean[n];
    int thisPt, closestPt = 0;
    double shortestDist;
    double distance;
    double thisPtx, thisPty, jPtx, jPty, a, b;
    k= n-1;
    int sum = (n-2)*(n-1)/ 2;


    thisPt = k;
    if(thisPt < 0) return;
    visited[thisPt] = true;
    path[0] = k;  // chose the starting city
    for(i=1; i<k; i++) {
      shortestDist = 0x1.fffffffffffffP+1023;
      thisPtx = t[thisPt].x;
      thisPty = t[thisPt].y;
      for(j=0; j<k; j++) {
        if (visited[j])
          continue;
        jPtx = t[j].x;
        jPty = t[j].y;
        a = (thisPtx-jPtx);
        b = (thisPty-jPty);
        distance = a * a + b * b;
        if (distance < shortestDist ){
          shortestDist = distance;
          closestPt = j;
        }
      }
      sum -= closestPt;
      path[i] = closestPt;
      visited[closestPt] = true;
      thisPt = closestPt;
    }
    path[k] = sum;
  }
}
