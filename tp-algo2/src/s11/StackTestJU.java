package s11;

import java.util.Random;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class StackTestJU {
  private IntStack s1, s2;
  private static Random rnd = new Random();

  @Before
  public void setUp() {
    s1 = new IntStack(10);
    s2 = new IntStack();
  }

  @Test
  public void testNewIsEmpty() {
    assertTrue(s1.isEmpty() && s2.isEmpty());
  }

  @Test
  public void testNotEmpty() {
    s1.push(4);
    s2.push(4);
    assertTrue(!s1.isEmpty() && !s2.isEmpty());
  }

  @Test
  public void testSize() {
    s1.push(1);
    s1.push(2);
    s1.push(3);
    s1.push(4);
    s1.push(5);
    s1.push(6);
    s1.push(7);
    s1.push(8);
    s1.push(9);
    s1.push(10);
    s1.push(11);
    s1.push(12);
    assertEquals(12, s1.pop());
    assertEquals(11, s1.pop());
    assertEquals(10, s1.pop());
    assertEquals(9, s1.pop());
    assertEquals(8, s1.pop());
    assertEquals(7, s1.pop());
    assertEquals(6, s1.pop());
    assertEquals(5, s1.pop());
    assertEquals(4, s1.pop());
    assertEquals(3, s1.pop());
    assertEquals(2, s1.pop());
    assertEquals(1, s1.pop());
  }

  @Test
  public void testPushThenPop() {
    s1.push(4);
    assertEquals(4, s1.pop());
  }
}
