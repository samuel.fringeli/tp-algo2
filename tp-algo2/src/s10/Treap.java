package s10;
import java.util.Random;
//--------------------------------------------------
public class Treap<E extends Comparable<E>>  {
  //============================================================
  static class TreapElt<E extends Comparable<E>> implements Comparable<TreapElt<E>> {
    static Random rnd=new Random();
    // -----------------------
    private final E elt;
    private int     pty;
    // -----------------------
    public TreapElt(E e) {
      elt=e;
      pty=rnd.nextInt();
    }

    public int pty() {
      return pty;
    }

    public E elt() {
      return elt;
    }

    public int compareTo(TreapElt<E> o) {
      return elt.compareTo(o.elt);
    }

    @Override public boolean equals(Object o) {
      if(o==null) return false;
      if (this.getClass() != o.getClass()) return false;
      if (elt==null) return false;
      return elt.equals(((TreapElt<?>)o).elt);
    }

    @Override public String toString() {
      return ""+elt+"#"+pty;
    }

    @Override public int hashCode() {
      return elt.hashCode();
    }
  }
  //============================================================
  private final BST<TreapElt<E>> bst;
  private int addC, rotA, remC, rotR, size;
  // --------------------------
  public Treap() {
    bst=new BST<>();
  }

  public void add(E e) {
    if (contains(e)) return;
    TreapElt<E> temp = new TreapElt<>(e);
    bst.add(temp);
    addC++;
    size++;
    BTreeItr<TreapElt<E>> itr = bst.locate(temp);
    percolateUp(itr);
  }

  public void remove(E e) {
    if (!contains(e)) return;
    BTreeItr<TreapElt<E>> itr = bst.locate(new TreapElt<>(e));
    remC++;
    size--;
    siftDownAndCut(itr);
  }

  public void printStats() {
    System.out.printf("Adds : %d, Rotations : %d", addC, rotA);
    System.out.printf("Removes : %d, Rotations removes : %d", remC, rotR);
  }

  public boolean contains(E e) {
    return bst.contains(new TreapElt<>(e));
  }

  public int size() {
    return size;
  }

  public E minElt() {
    return bst.minElt().elt;
  }

  public E maxElt() {
    return bst.maxElt().elt;
  }

  public String toString() {
    return bst.toString();
  }

  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  private void siftDownAndCut(BTreeItr<TreapElt<E>> ti) {
    if (ti.isLeafNode()) {
      ti.cut();return;
    }
    if (!ti.hasRight()) {
      ti.paste(ti.left().cut());return;
    }
    if (!ti.hasLeft()) {
      ti.paste(ti.right().cut());return;
    }
    if (isLess(ti.left(),ti.right())) {
      ti.rotateRight();
      rotR++;
      siftDownAndCut(ti.right());
    }else {
      ti.rotateLeft();
      rotR++;
      siftDownAndCut(ti.left());
    }
  }

  private void percolateUp(BTreeItr<TreapElt<E>> ti) {
    while((!ti.isRoot()) && isLess(ti, ti.up())) {
      if (ti.isLeftArc()) {
        ti=ti.up(); ti.rotateRight();
      }
      else {
        ti=ti.up(); ti.rotateLeft();
      }
      rotA++;
    }
  }

  private boolean isLess(BTreeItr<TreapElt<E>> a, BTreeItr<TreapElt<E>> b) {
    TreapElt<E> ca= a.consult();
    TreapElt<E> cb= b.consult();
    return ca.pty()<cb.pty();
  }
}
