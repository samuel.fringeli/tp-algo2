package s21;
import java.awt.Point;

public class Geom {

  public static int signedArea(Point p1, Point p2, Point p3) {
    long p1_x=p1.x, p1_y=p1.y, p2_x=p2.x, p2_y=p2.y, p3_x=p3.x, p3_y=p3.y;
    long area = (p2_x-p1_x)*(p3_y-p1_y) - (p3_x-p1_x)*(p2_y-p1_y);
    if (area > Integer.MAX_VALUE) area = Integer.MAX_VALUE;
    if (area < Integer.MIN_VALUE) area = Integer.MIN_VALUE;
    return (int)area;
  }

  public static int ccw(Point p1, Point p2, Point p3) {
    int a = signedArea(p1, p2, p3);
    if (a < 0) return -1;
    if (a > 0) return +1;
    return 0;
  }

  public static boolean intersect(Segm s0, Segm s1) {
    int s0_s1from_ccw = ccw(s0.from(), s0.to(), s1.from());
    int s0_s1to_ccw = ccw(s0.from(), s0.to(), s1.to());
    int s1_s0from_ccw = ccw(s1.from(), s1.to(), s0.from());
    int s1_s0to_ccw = ccw(s1.from(), s1.to(), s0.to());

    // Traitement particulier lorsque les 2 segments sont alignés
    if (s0_s1from_ccw == 0 && s0_s1to_ccw == 0 && s1_s0from_ccw == 0  && s1_s0to_ccw == 0)
      return isOnSegment(s0.from(), s1) || isOnSegment(s0.to(), s1)
              || isOnSegment(s1.from(), s0) || isOnSegment(s1.to(), s0);

    return ((s0_s1from_ccw * s0_s1to_ccw) <= 0)
            && ((s1_s0from_ccw * s1_s0to_ccw) <= 0);
  }

  public static boolean isInLeftAngle(Point query, Point a, Point b, Point c) {
    boolean ab_ccw = ccw(a, b, query) >= 0;
    boolean bc_ccw = ccw(b, c, query) >= 0;
    return ccw(a, b, c) >= 0 ? ab_ccw && bc_ccw : ab_ccw || bc_ccw;
  }

  public static boolean isInTriangle(Point query, Point a, Point b, Point c) {
    // Soit le point donne le même ccw pour tous les côtés du triangle, soit il se trouve sur un des côtés
    return ccw(a, b, query) == ccw(b, c, query) && ccw(a, b, query) == ccw(c, a, query)
            || isOnSegment(query, new Segm(a, b))
            || isOnSegment(query, new Segm(b, c))
            || isOnSegment(query, new Segm(c, a));
  }

  public static boolean isOnSegment(Point p, Segm s) {
    // Vérifie que les 3 points sont alignés, et que p est inclut dans le rectangle décrit par s
    return ccw(s.from(), s.to(), p) == 0
            && (p.x >= s.from().x && p.x <= s.to().x || p.x <= s.from().x && p.x >= s.to().x)
            && (p.y >= s.from().y && p.y <= s.to().y || p.y <= s.from().y && p.y >= s.to().y);
  }

  public static boolean isInCcwOrder(Point[] simplePolygon) {
    int n = simplePolygon.length;

    // Trouver le point extrême selon le critère : d'abord comparer les x, et comparer les y en cas d'égalité
    int best = -1;
    for (int i = 0; i < n; i++)
      if (best == -1 || isHorizSmaller(simplePolygon[i], simplePolygon[best]))
        best = i;

    return ccw(simplePolygon[(best-1+n)%n], simplePolygon[best], simplePolygon[(best+1)%n]) > 0;
  }

  public static boolean isHorizSmaller(Point a, Point b) {
    return (a.x<b.x) || (a.x==b.x && a.y<b.y);
  }

  public static boolean isInPolygon(Point [] polyg, Point p) {
    int n = polyg.length; // Nombre de sommets du polygone

    boolean odd = false; // Indique si le nombre de segments traversés est impair
    Segm string = new Segm(p, new Point(p.x, Integer.MIN_VALUE)); // Fil pour l'algorithme du "fil à plomb"

    // Avant de commencer, s'assurer que le point p ne se trouve pas sur un des segments du polygone
    // (si c'est le cas on dit que le point p est forcément dans le polygone)
    for (int i = 0; i < n; i++) {
      Segm segm = new Segm(polyg[i], polyg[(i+1)%n]);
      if (isOnSegment(p, segm))
        return true;
    }

    // Ensuite, trouver un point qui n'est pas sur le fil avec lequel on pourra commencer l'algorithme
    int j = -1;
    for (int i = 0; i < n; i++) {
      if (!isOnSegment(polyg[i], string)) {
        j = i;
        break;
      }
    }

    // Si il n'y en a pas, alors tous les sommets du polygone sont sur le fil et il a donc une aire de 0, et puisqu'on
    // sait que le point p n'est pas sur un des segments alors on sait qu'il n'est pas dans le polygone
    if (j == -1)
      return false;

    // Puis on traite chaque sommet du polygone dans l'ordre à partir de j
    for (int i = 0; i < n; i++, j++) {

      // Traitement particulier si le sommet actuel est sur le fil
      if (isOnSegment(polyg[mod(j, n)], string)) {
        int before = polyg[mod(j-1, n)].x - p.x; // Seul le signe de before nous intéresse, il indique si le point
        // précédent dans le polygone se trouve à gauche ou à droite de p

        // On passe les sommets qui sont sur le fil
        // (toute la suite de segments verticaux sont traités comme un seul grand segment)
        while (isOnSegment(polyg[mod(j+1, n)], string)) {
          i++;
          j++;
        }

        int after = polyg[mod(j+1, n)].x - p.x; // Seul le signe de after nous intéresse, similaire à before

        // Si after n'est pas du même côté que before, alors on comptabilise une traversée
        if (before * after < 0)
          odd = !odd;
      }

      // Traitement normal : si le segment après le sommet actuel traverse le fil, alors on comptabilise une traversée
      if (intersect(string, new Segm(polyg[mod(j, n)], polyg[mod(j+1, n)])))
        odd = !odd;
    }

    return odd;
  }

  // a mod b
  public static int mod(int a, int b) {
    while (a < 0)
      a += b;
    return a % b;
  }

  public static void main(String[] args) {
    s21.CG.main(args);
  }

}
