package s07;

import java.util.Arrays;
import java.util.BitSet;

// ------------------------------------------------------------
public class DisjointSets {
  private int nbOfElements = 0;
  private int[] parent;
  private int nbOfRoots = nbOfElements;
  private int[] min;

  public DisjointSets(int nbOfElements) {
    this.parent = new int[nbOfElements];
    this.nbOfRoots = nbOfElements;
    this.nbOfElements = nbOfElements;
    this.min = new int[nbOfElements];

    // Initialisation of parent[] et min[]
    for (int i=0; i < nbOfElements; i++) {
      min[i] = -i;
      parent[i] = -1;
    }
  }

  private int root(int elt) {
    if (parent[elt] < 0) return elt;
    int parentRoot = root(parent[elt]);
    parent[elt] = parentRoot;
    return parentRoot;
  }

  private void mergeRoots(int r1, int r2) {
    if (r1 == r2) return;
    int x = parent[r1] < parent[r2] ? r1:r2;
    int y = parent[r1] < parent[r2] ? r2:r1;

    parent[x] += parent[y];
    parent[y] = x;
    min[x] = Math.max(min[x], min[y]);
    nbOfRoots--;
  }

  // PRE: 0 <= i < nbOfElements and 0 <= j < nbOfElements
  public boolean isInSame(int i, int j) {
    return root(i)==root(j);
  }

  // PRE: 0 <= i < nbOfElements and 0 <= j < nbOfElements
  public void union(int i, int j) {
    mergeRoots(root(i), root(j));
  }

  public int nbOfElements() {  // as given in the constructor
    return nbOfElements;
  }

  // PRE: 0 <= i < nbOfElements and 0 <= j < nbOfElements
  public int minInSame(int i) {
    return min[root(i)];
  }

  public boolean isUnique() {
    return nbOfRoots == 1;
  }

  @Override
  public String toString() {
    if (nbOfElements == 0) return "nothing to display";

    StringBuilder result = new StringBuilder("{");
    BitSet elementsShown = new BitSet();
    int currentRoot = root(0);
    int currentElementNumber = 0;

    while(currentElementNumber < nbOfRoots) {
      for (int i = 0; i < nbOfElements; i++) {
        if (isInSame(currentRoot, i)) {
          result.append(i).append(",");
          elementsShown.set(i);
        }
      }

      for (int i = 0; i < nbOfElements; i++) {
        if (!elementsShown.get(i)) {
          currentRoot = root(i);
          break;
        }
      }
      currentElementNumber++;

      result.append(currentElementNumber == nbOfRoots ? "}":"},{");
    }
    return result.toString();
  }

  @Override
  public boolean equals(Object otherDisjSets) {
    if (otherDisjSets == null) return false;
    if (otherDisjSets.getClass() != DisjointSets.class) return false;

    if (this.nbOfElements != ((DisjointSets)otherDisjSets).nbOfElements()) return false ;

    for (int i = 0; i < nbOfElements; i++) {
      if (minInSame(i) != ((DisjointSets)otherDisjSets).minInSame(i)) return false;
    }

    return true ;
  }
}
