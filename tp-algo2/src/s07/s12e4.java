package s07;

import java.util.Arrays;

public class s12e4 {
    public static void main(String[] args) {
        int[] array = {50,30,5,1,11,10,60};

        // Creation of couple values (smaller, bigger) in N/2 comparison
        for (int i = 0; i < array.length; i += 2) {
            if (i != array.length - 1) {
                if (array[i] > array[i+1]){
                    int temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                }
            }
        }

        int[] couple = {array[0], array[1]};

        // Comparison of the couples and set each time the min and the max (N-2 even elements, N-1 odd)
        for (int i = 0; i < array.length; i += 2) {
            if (i != array.length - 1) {
                couple[1] = Math.max(couple[1], array[i + 1]);
            } else {
                couple[1] = Math.max(couple[1], array[i]);
            }
            couple[0] = Math.min(couple[0], array[i]);
        }
    }
}
