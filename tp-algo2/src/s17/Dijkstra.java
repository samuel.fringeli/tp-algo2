package s17;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

// ------------------------------------------------------------
public class Dijkstra {
  // ------------------------------------------------------------
  static class Vertex implements Comparable<Vertex> {
    public final int vid;
    public final int pty;
    public Vertex(int vid, int pty) {
      this.vid=vid; this.pty=pty;
    }
    @Override public int compareTo(Vertex v) {
      return Integer.compare(this.pty,  v.pty);
    }
  }

  static class PtyElt<T> implements Comparable<PtyElt<T>> {
    int pty;
    T elt;
    public PtyElt(int pty, T elt) {
      this.pty = pty;
      this.elt = elt;
    }
    @Override
    public int compareTo(PtyElt<T> e) {
      return pty - e.pty;
    }
  }
  // ------------------------------------------------------------
  // POST : minDist[i] is the min distance from a to i
  //                      MAX_VALUE if i is not reachable, 0 for a
  //         parent[i] is the parent of i in the corresponding tree
  //                      -1 if i is not reachable, and for a
  public static void dijkstra(WeightedDiGraph g, int a, int[] minDist, int[] parent) {
    dijkstra(g, a, minDist, parent, false);
  }

  public static void dijkstra(WeightedDiGraph g, int a, int[] minDist, int[] parent, boolean invert) {
    int n = minDist.length;
    boolean[] isVisited = new boolean[n];
    for (int i = 0; i < n; i++) {
      minDist[i] = Integer.MAX_VALUE;
      parent[i] = -1;
    }

    PriorityQueue<PtyElt<Integer>> priorityQueue = new PriorityQueue<>();
    minDist[a]= 0;

    priorityQueue.add(new PtyElt<>(minDist[a], a));
    while (!priorityQueue.isEmpty()) {
      int k = priorityQueue.poll().elt;
      if (isVisited[k]) continue;
      isVisited[k]= true;
      for (int i: (invert ? g.neighboursTo(k) : g.neighboursFrom(k))) {
        int va = invert ? i : k;
        int vb = invert ? k : i;
        if (minDist[i] > minDist[k]+g.edgeWeight(va, vb)) {
          minDist[i] = minDist[k]+g.edgeWeight(va, vb);
          parent[i] = k;
        }
        priorityQueue.add(new PtyElt<>(minDist[i], i));
      }
    }
  }

  // ------------------------------------------------------------
  static Set<Integer> strongComponentOf(WeightedDiGraph g, int vid) {
    Set<Integer> res=new HashSet<>();
    int n = g.nbOfVertices();

    int[] from = new int[n];
    dijkstra(g, vid, new int[n], from, false);

    int[] to = new int[n];
    dijkstra(g, vid, new int[n], to, true);

    res.add(vid);
    for (int i = 0; i < n; i++)
      if (from[i] != -1 && to[i] != -1)
        res.add(i);

    return res;
  }

  // ------------------------------------------------------------
  public static void main(String [] args) {
    int nVertices = 6; // int nEdges = 12;
    final int A=0, B=1, C=2, D=3, E=4, F=5;
    int[] srcs  = {A, A, A, B, B, D, D, D, D, E, F, F };
    int[] dsts  = {B, C, F, F, C, A, B, C, E, A, D, E };
    int[] costs = {12,6, 14,1, 7, 9, 3, 2, 4, 5, 10,11};

    WeightedDiGraph g = new WeightedDiGraph(nVertices, srcs, dsts, costs);
    System.out.println("Input Graph: " + g);

    int n = g.nbOfVertices();
    int[] minCost = new int[n];
    int[] parent  = new int[n];
    for (int a=0; a<n; a++) {
      dijkstra(g, a, minCost, parent);
      System.out.println("\nMinimal distances from " +a); 
      for (int i=0; i<minCost.length; i++) {
        String s="to "+ i +":";
        if (minCost[i]==Integer.MAX_VALUE) s+=" unreachable";
        else s+= " total " + minCost[i] +", parent "+parent[i];
        System.out.println(s);
      }
    }
  }
}
