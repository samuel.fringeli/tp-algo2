package s12;
import static s12.MemoryManager.NIL;


public final class XORListItr<E> implements IListItr<E> {
  final XORList<E> list;
  // These are plain references (not XOR-ed):
  int pred, succ;
  // But MemoryManager.next(...) are XOR-ed links!!

  XORListItr(XORList<E> theList) {
    list = theList;
    goToFirst();
  }

  @Override
  public void insertAfter(E e) {
    int index = list.mm.allocate();
    list.mm.setData(index, e);
    int oldSucc = succ;
    succ = index;
    list.mm.setNext(succ, oldSucc ^ pred); if (pred == NIL) {
      list.first = index; } else
      list.mm.setNext(pred, list.mm.next(pred) ^ oldSucc ^ succ);
    if (oldSucc == NIL) {
      list.last = index;
    } else {
      list.mm.setNext(oldSucc, list.mm.next(oldSucc) ^ pred ^ succ);
    }
    list.size++;
    assert consultAfter() == e;
  }

  @Override
  public void removeAfter() {
    if (isLast())
      throw new IllegalStateException("can't removeAfter when isLast");

    int oldSucc = succ;
    succ = list.mm.next(succ) ^ pred;

    if (succ == NIL) {
      list.last = pred;
    } else {
      list.mm.setNext(succ, list.mm.next(succ) ^ oldSucc ^ pred);
    }

    if (pred == NIL) {
      list.first = succ;
    } else {
      list.mm.setNext(pred, list.mm.next(pred) ^ oldSucc ^ succ);
    }

    list.size--;
    list.mm.deallocate(oldSucc);
  }

  @Override
  public E consultAfter() {
    if (isLast())
      throw new IllegalStateException("can't consultAfter when isLast");
    return list.mm.data(succ);
  }

  @Override
  public void goToNext() {
    if(isLast()) throw new IllegalStateException("can't goToNext when isLast");

    int oldPred = pred;
    pred = succ;
    succ = list.mm.next(succ) ^ oldPred;
  }

  @Override
  public void goToPrev() {
    if(isFirst()) throw new IllegalStateException("can't goToPrev when isFirst");

    int oldSucc = succ;
    succ = pred;
    pred = list.mm.next(pred) ^ oldSucc;
  }

  @Override
  public void goToFirst() {
    pred = NIL;
    succ = list.first;
  }

  @Override
  public void goToLast() {
    succ = NIL;
    pred = list.last;
  }

  @Override
  public boolean isFirst() {
    return pred == NIL;
  }

  @Override
  public boolean isLast()  {
    return succ == NIL;
  }
}
