package s07;

import java.util.Arrays;

public class s12e10 {
    public static void main(String[] args) {
        int[][] polygone = {{1,4},{3,5},{5,4},{4,1},{2,1}};
        double[] nextSegmentSize = new double[polygone.length];
        double perimeter = 0;

        for (int i = 0; i < polygone.length; i++) {
            if (i != polygone.length - 1) {
                nextSegmentSize[i] = Math.sqrt(
                        Math.pow((polygone[i][0]-polygone[i+1][0]),2) +
                                Math.pow((polygone[i][1]-polygone[i+1][1]),2)
                );
            }else {
                nextSegmentSize[i] = Math.sqrt(
                        Math.pow((polygone[i][0]-polygone[0][0]),2) +
                                Math.pow((polygone[i][1]-polygone[0][1]),2)
                );
            }
            perimeter+= nextSegmentSize[i];
        }

        // We haven't found a way to find the couple of points in O(n). One idea would have been
        // to test all possible pairs of points and choose the one with the most equitable
        // separation, but it is not in O(n).


        System.out.println(Arrays.toString(nextSegmentSize));
        System.out.println(perimeter);
    }
}
