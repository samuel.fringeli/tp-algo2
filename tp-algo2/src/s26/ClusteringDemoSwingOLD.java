package s26;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.Random;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class ClusteringDemoSwingOLD extends JFrame {
  List<Cluster> clusters;
  List<Point> points;
  // ------------------------------------------------------------
  public ClusteringDemoSwingOLD(String title) {
    super(title);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
  }
  // ------------------------------------------------------------
  public void showSolution(List<Point> pts, List<Cluster> c) {
    clusters = c; points=pts;
    repaint();setVisible(true);
  }
  // ------------------------------------------------------------
  public void paint( Graphics g ) {
    Color[] colors= {Color.GREEN, Color.BLUE, Color.ORANGE, Color.PINK};
    Graphics2D g2 = (Graphics2D) g;
    if (clusters!=null) {
      for (int i=0; i<clusters.size(); i++) {
        g2.setColor(colors[i%colors.length]);
        Cluster c = clusters.get(i);
        for (Point p: c.pts) {
          g2.drawLine(c.x, c.y, p.x, p.y);
        }
      }
    }
    g2.setColor(Color.BLACK);
    for (Point p: points) {
      g2.drawOval(p.x, p.y, 3, 3);
    }
  }
  // ------------------------------------------------------------
  static List<Point> pointsWithin(int xMin, int xMax,
                                  int yMin, int yMax, int nbOfPoints, Random r) {
    HashSet<Point> res=new HashSet<Point>();
    while(res.size()<nbOfPoints) {
      int x= xMin+4*r.nextInt((xMax-xMin)/4);
      int y= yMin+4*r.nextInt((yMax-yMin)/4);
      res.add(new Point(x,y));
    }
    Vector<Point> res1=new Vector<Point>(res);
    return res1;
  }
  // ------------------------------------------------------------
  static List<Point> onePointsSet() {
    Random r = new Random();
    Vector<Point> pts = new Vector<Point>();
    pts.addAll(pointsWithin(20, 100,  120, 200,  30, r));
    pts.addAll(pointsWithin(200, 300, 120, 200,  45, r));
    pts.addAll(pointsWithin(80, 220, 30, 80,  70, r));
    return pts;
  }
  // --------------------------------------------------
  public static void main( String [ ] args ) {
    List<Point> pts = onePointsSet();
    System.out.println("points chosen..."); System.out.flush();
    List<Cluster> clusters=null;
    ClusteringDemoSwingOLD f;
    int nbClusters = 3;
    f = new ClusteringDemoSwingOLD("Input"); f.setSize(500, 300); f.setLocation(250,0);
    f.showSolution(pts, null);

    clusters = Clustering.initKMeans(pts, nbClusters);
    Clustering.kMeans(clusters, pts);
    f = new ClusteringDemoSwingOLD("K-Means "); f.setSize(500, 300); f.setLocation(0,300);
    f.showSolution(pts, clusters);

    clusters = Clustering.initHierarchical(pts);
    Clustering.hierarchical(clusters, nbClusters);
    f = new ClusteringDemoSwingOLD("Hierarchichal"); f.setSize(500, 300);f.setLocation(500,300);
    f.showSolution(pts, clusters);
  }
}
