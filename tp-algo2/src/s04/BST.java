package s04;
// =======================
public class BST<E extends Comparable<E>> {
  protected BTree<E> tree;
  protected int   crtSize;

  public BST() {
    tree = new BTree<E>();
    crtSize = 0;
  }

  public BST(E [] tab) {  // PRE sorted, no duplicate
    tree = tab.length == 0 ? new BTree<E>() : optimalBST(tab, 0, tab.length - 1);
    crtSize = tab.length;
  }

  /**
   * returns where e is, or where it should be inserted as a leaf
   */
  protected BTreeItr<E> locate(E e) {
    BTreeItr<E> itr = new BTreeItr<>(tree);
    if (tree.isEmpty()) return itr;
    while (e.compareTo(itr.consult()) != 0) {
      if (e.compareTo(itr.consult()) < 0) itr = itr.left();
      else itr = itr.right();
      if (itr.isBottom()) return itr;
    }
    return itr;
  }

  public void add(E e) {
    BTreeItr<E> locatedElement = this.locate(e);
    if (this.contains(e)) return;
    locatedElement.insert(e);
    crtSize++;
  }

  public void remove(E e) {
    BTreeItr<E> itr = locate(e);
    if (this.contains(e)) {
      while (itr.hasRight()) {
        itr.rotateLeft();
        itr = itr.left();
      }
      BTree<E> treeToReplace = itr.cut();
      BTreeItr<E> treeToReplaceItr = new BTreeItr<>(treeToReplace);
      if (!treeToReplaceItr.isLeafNode()) itr.paste(treeToReplaceItr.left().cut());
      crtSize--;
    }
  }

  public boolean contains(E e) {
    BTreeItr<E> ti = locate(e);
    return ! ti.isBottom();
  }

  public int size() {
    return crtSize;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public E minElt() {
    return (new BTreeItr<E>(tree)).leftMost().consult();
  }

  public E maxElt() {
    return (new BTreeItr<E>(tree)).rightMost().consult();
  }

  @Override public String toString() {
    return ""+tree;
  }

  public String toReadableString() {
    String s = tree.toReadableString();
    s += "size=="+crtSize+"\n";
    return s;
  }

  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  // PRE : sorted must not be empty
  private BTree<E> optimalBST(E[] sorted, int left, int right) {
    BTree<E> r = new BTree<E>();
    BTreeItr<E> itr = r.root();

    int middle = (right - left) / 2 + left;
    itr.insert(sorted[middle]); // insert middle in root

    if (left != right) {
      itr.left().paste(optimalBST(sorted, left, middle)); // left part of tree
      itr.right().paste(optimalBST(sorted, middle + 1, right)); // right part of tree
    }
    return r;
  }
}
