package s12;

import java.util.*;
import java.util.function.Supplier;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/* IntelliJ indique que 100% des méthodes et 93% des lignes sont couvertes */
public final class IMemoryManagerJU {
    static Random r = new Random();
    IMemoryManager<Integer> memoryManager;
    int memoryManagerLength = 100;

    @Before
    public void initialize() {
        memoryManager = new MemoryManager<>();
    }

    @Test
    public void testAddValues() {
        ArrayList<Integer> indexes = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();

        for (int i = 0; i < memoryManagerLength; i++) {
            indexes.add(memoryManager.allocate());
            int value = r.nextInt();
            values.add(i, value);
            memoryManager.setData(indexes.get(i), value);
        }

        for (int i = 0; i < indexes.size(); i++) {
            // verify that values matches in memory manager
            assertTrue(values.get(i).equals(memoryManager.data(indexes.get(i))));
        }
    }

    @Test
    public void testNextValues() {
        ArrayList<Integer> indexes = getRandomArrayListOfIndexes();

        for (int i = 0; i < indexes.size(); i += 2) {
            memoryManager.setNext(indexes.get(i), indexes.get(i + 1));
        }

        for (int i = 0; i < indexes.size(); i += 2) {
            // test if size hasn't changed when setNext
            assertTrue(checkMemoryManagerSize(indexes.get(i), 2));
        }
    }

    @Test
    public void checkIfIsAllDealocated() {
        ArrayList<Integer> indexes = getRandomArrayListOfIndexes();

        for (int i = 1; i < indexes.size(); i += 2) {
            memoryManager.deallocate(indexes.get(i));
            assertTrue(memoryManager.data(indexes.get(i)) == null);
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void allocateBranchException() {
        getRandomArrayListOfIndexes();

        memoryManager.deallocate(22);
        memoryManager.deallocate(-1);
        int i = memoryManager.allocate();
    }

    @Test(expected = AssertionError.class)
    public void setNextBranchException() {
        getRandomArrayListOfIndexes();

        memoryManager.setNext(100, 99);
    }

    @Test
    public void printTest() {
        String res1 = memoryManager.toString();
        String expected = " 0, 1, \n" +
                "[1, -1]\n" +
                "  firstFreeCell=0";
        assertEquals(res1, expected);

        ArrayList<Integer> index = new ArrayList<>(); for (int i = 0; i < memoryManagerLength; i++) {
            memoryManager.setData(memoryManager.allocate(), i); }
        String res2 = memoryManager.toString();
        assertNotEquals(res2, expected);
    }

    public ArrayList<Integer> getRandomArrayListOfIndexes() {
        ArrayList<Integer> indexes = new ArrayList<>();

        for (int i = 0; i < memoryManagerLength; i++) {
            indexes.add(memoryManager.allocate());
            memoryManager.setData(indexes.get(i), r.nextInt());
        }
        return indexes;
    }

    private boolean checkMemoryManagerSize(int start, int expectedSize) {
        int size = 0;
        int counter = start;
        while (counter >= 0 && expectedSize > size) {
            counter = memoryManager.next(counter);
            size++;
        }
        return expectedSize == size;
    }
}
